const colyseus = require("colyseus");
const http = require("http");

let ID = 1;

class CoinsRoom extends colyseus.Room {

    onInit() {
        this.setState({
            coins: [],
            heros: [],

            herosByClientId: {}
        });
        this.clock.setInterval(this.spawnCoin.bind(this), 4000);
        this.clock.start();
    }

    spawnCoin() {
        if (!this.state.heros.length) return;
        this.state.coins.push({
            id: ID++,
            x: (Math.random()*255)|0,
            y: (Math.random()*255)|0
        });
    }

    onJoin(client, {type, color, x, y}) {
        let id = ID++;
        let hero = {
            id, type, color, x, y, gainedCoins: 0
        };
        this.state.herosByClientId[client.sessionId] = hero;
        this.state.heros.push(hero);

        this.send(client, {type: "id", id});
    }

    onLeave(client) {
        let hero = this.state.herosByClientId[client.sessionId];
        if (hero) {
            this.state.heros.splice(this.state.heros.indexOf(hero));
        }
    }

    onMessage(client, message) {
        let hero = this.state.herosByClientId[client.sessionId];
        switch (message.type) {
            case "move":
                hero.x = message.x;
                hero.y = message.y;
                break;
            case "trygain":
                //todo: need to check overlaping here!! not on client!
                let coin = this.state.coins.find(c => c.id === message.coinId);
                if (coin) {
                    this.state.coins.splice(this.state.coins.indexOf(coin), 1);
                    hero.gainedCoins++;
                }
                break;
        }
    }

    onDispose() {
        this.clock.stop();

    }
}


const gameServer = new colyseus.Server({
    server: http.createServer()
});

gameServer.register("coins", CoinsRoom);
gameServer.listen(9999);
