const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const PHASER_DIR = path.join(__dirname, '/node_modules/phaser-ce/');

module.exports = {
	devtool: 'source-map',
	entry: {
		'index': './src/index.js'
	},
	resolve: {
		modules: [
			path.join(__dirname, 'src'),
			'node_modules'
		],
		alias: {
			phaser: path.resolve(PHASER_DIR, 'build/custom/phaser-split.js'),
			pixi: path.resolve(PHASER_DIR, 'build/custom/pixi.js'),
			p2: path.resolve(PHASER_DIR, 'build/custom/p2.js'),
		},
	},

	output: {
		path: path.join(__dirname, 'build'),
		filename: '[name].js'
	},
	plugins: [
		new webpack.LoaderOptionsPlugin({
			debug: true
		}),
		new CopyWebpackPlugin([
			{ from: 'src/assets', to: 'assets' },
			{ from: 'src/index.html' },
			{ from: 'src/main.css' },
		])
	],
	module: {
		rules: [
			// ES6 transpiler
			{
				test: /\.js$/,
				use: [{
					loader: 'babel-loader',
					options: { presets: ['env'], plugins: ["transform-object-rest-spread"] },
				}],
				include: path.resolve(__dirname, "src")
			},
			{
				test: /pixi\.js/,
				use: [{
					loader: 'expose-loader',
					options: 'PIXI',
				}],
			},
			{
				test: /phaser-split\.js$/,
				use: [{
					loader: 'expose-loader',
					options: 'Phaser',
				}],
			},
			{
				test: /p2\.js/,
				use: [{
					loader: 'expose-loader',
					options: 'p2',
				}],
			},
			// Static files
			{
				test: /\.html$/,
				loader: 'static',
				include: path.resolve(__dirname, "src")
			},
			{
				test: /\.json$/,
				loader: 'static',
				include: path.resolve(__dirname, "src")
			}
		]
	}
}
