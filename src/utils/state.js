export class StateBase {
    constructor(id, base) {
        this.id = id || createId(base);
        this._base = base;

        this._callbacks = [];
        this._newValuesRequestedThisTick = [];
    }

    postConstruct() {
        this.newValueOf = wrapObject(this, this).newValueOf;
        return this;
    }

    alias(alias, propertyName) {
        Object.defineProperty(this, alias, {
            get() { return this[propertyName]; },
            set(newVal) {
                this[propertyName] = newVal;
            }
        })
    }

    save() {
        game.level.state.objects[this.id] = this;
        if (this._base) {
            game.level.state.objects[this._base] = game.level.state.objects[this._base] || [];
            game.level.state.objects[this._base].push(this);
        }
        return this;
    }

    //same as onChange, but resets flag to false after raising
    onEvent(name, changeCallback) {
        this._callbacks.push({checkFn: n => n[name], changeCallback: newVal => {
            if (newVal) {
                changeCallback();
            }
        }, afterCallback: () => {
            this[name] = false;
        }});
    }

    onChange(checkFn, changeCallback) {
        this._callbacks.push({checkFn, changeCallback})
    }

    ifChanged(checkFn, changeCallback) {
        let newValueHolder = checkFn(this.newValueOf);
        let newValue = newValueHolder.getNewValue();
        if (newValue !== UNCHANGED) {
            changeCallback(newValue);
        }
        this._newValuesRequestedThisTick.push(newValueHolder);
    }

    checkChanges() {
        let callbacksToProcess = this._callbacks.slice();
        for (let cp of callbacksToProcess) {
            let newValueHolder = cp.checkFn(this.newValueOf);
            if (!newValueHolder || !newValueHolder.getNewValue) console.error(cp.checkFn.toString());
            let newValue = newValueHolder.getNewValue();
            cp.valueToPass = newValue;
            if (newValue !== UNCHANGED) {
                this._newValuesRequestedThisTick.push(newValueHolder);
            }
        }
        for (let t of this._newValuesRequestedThisTick) {
            t.resetChange();
        }
        for (let cp of callbacksToProcess) {
            if (cp.valueToPass !== UNCHANGED) {
                cp.changeCallback(cp.valueToPass)
            }
            if (cp.afterCallback) cp.afterCallback();
        }
        this._newValuesRequestedThisTick = [];
    }
}


export function initState(level, state) {
    game.level = level;
    game.level.state = Object.assign({}, state, {objects: {}, sprites: {}});
    ID = 1;
    return game.level.state;
}

let ID = 1;

export function createId(base = "object") {
    return base + ID++;
}

export const UNCHANGED = null;

export function wrapObject(obj, replacement = {}) {
    let objCopy = Object.assign({}, obj);

    let oldValues = {};
    let newValueOf = {};
    for (let name of Object.getOwnPropertyNames(objCopy)) {
        let val = objCopy[name];
        if (!Array.isArray(val) && typeof val === "object") {
            //need to wrap too
            let wrapped = wrapObject(val);
            replacement[name] = wrapped.replacement;
            newValueOf[name] = wrapped.newValueOf;
        } else {
            replacement[name] = val;
            oldValues[name] = val;
            newValueOf[name] = {
                getNewValue() {
                    if (oldValues[name] !== replacement[name]) {
                        return replacement[name];
                    }
                    return UNCHANGED;
                },

                resetChange() {
                    oldValues[name] = replacement[name];
                }
            };
        }
    }

    return {
        newValueOf, replacement
    }

}