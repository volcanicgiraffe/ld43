import { CAMPAIGN } from "../consts";

const PADDING_TOP = 250;
const ITEMS_PADDING = 42;

export default {
    create() {
        game.stage.backgroundColor = '#0F0000';
        game.world.setBounds(0, 0, 1280, 720);
        game.add.tween(game.world).to({ alpha: 1 }, 100, null, true);

        this.bg = game.add.sprite(0, 0, 'title');
        this.bg.width = game.world.width;

        this.help = game.add.sprite(0, 0, 'help');
        this.help.anchor.set(0.5);
        this.help.x = game.world.width / 2 - 55;
        this.help.y = game.world.height / 2;
        this.help.visible = false;


        this.tooltip = game.add.bitmapText(game.world.width - 60, game.world.height - 110, "font36", '', 36);
        this.tooltip.tint = 0xBBBBBB;
        this.tooltip.anchor.set(1, 0);
        //
        this.cursors = game.input.keyboard.createCursorKeys();
        this.keys = {
            space: game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR),
            enter: game.input.keyboard.addKey(Phaser.Keyboard.ENTER),
        };

        this._showMenu(this._initialMenu());

        this._theme = game.add.sound('aracnho-2');
        this._theme.play(undefined, undefined, 0, true);
        this._theme.volume = 0.5;

        if (typeof ga === "function") ga('send', 'event', 'state', 'menu');
    },

    _showMenu(items) {
        this.selectedItem = 0;

        this.items = items;

        if (this.labels && this.labels.length) for (let label of this.labels) label.destroy();
        if (this.pointer) this.pointer.destroy();

        this.labels = [];

        let startY = (game.world.height - (this.items.length) * ITEMS_PADDING) / 2;

        for (let i = 0; i < this.items.length; i++) {
            let item = this.items[i];
            let label = game.add.bitmapText(game.world.width - 140, startY + i * ITEMS_PADDING, "font36", item.text, 36);
            label.anchor.set(1, 0);
            label.tint = item.devItem ? 0xBBBBBB : 0xFFFFFF;
            label.itemIndex = i;
            label.inputEnabled = true;
            label.events.onInputDown.add(this._labelClick, this);
            label.events.onInputOver.add(this._labelHover, this);

            this.labels.push(label);
        }

        this.pointer = game.add.bitmapText(game.world.width - 120, this.labels[0].y, "font36", '<', 36);

        this.pointer.tint = 0xFFFFFF;
        game.add.tween(this.pointer).to({ x: this.pointer.x - 3 }, 300, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
    },

    _labelClick(label) {
        this.items[label.itemIndex].action();
        game.sounder.play('click_1');
    },

    _labelHover(label) {
        this.selectedItem = label.itemIndex;
    },


    _initialMenu() {
        this.help.visible = false;
        let items = [
            {
                text: game.storage.loadLevelIndex() > 0 ? "Continue" : "New Game",
                action: () => {
                    if (typeof ga === "function") ga('send', 'event', 'menu_navigation', game.storage.loadLevelIndex() > 0 ? "continue" : "new_game");

                    game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
                        game.state.start("level_state");
                    });
                },
                tooltip: 'Continue from the last level'
            },
        ];

        if (game.storage.loadLevelIndex() > 0) {
            items.push({
                text: "New Game",
                action: () => {
                    this._showMenu(this._confirmationNewGame());
                },
                tooltip: 'Reset the game progress and start all over again'
            });
        }

        items.push({
            text: "Level Select",
            action: () => {
                this._showMenu(this._levelSelectMenu());
            },
            tooltip: 'Pick any level to play'
        });

        items.push({
            text: "Help",
            action: () => {
                this._showMenu(this._helpMenu());
            },
            tooltip: ''
        });

        return items;
    },

    _confirmationNewGame() {
        let items = [
            {
                text: "Are you sure?",
                action: () => {
                    game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
                        if (typeof ga === "function") ga('send', 'event', 'menu_navigation', 'yep_i_am_sure');
                        game.storage.resetProgress();
                        game.state.start("level_state");
                    });
                },
                tooltip: 'This will erase all game progress'
            },
            {
                text: "Back",
                action: () => {
                    if (typeof ga === "function") ga('send', 'event', 'menu_navigation', 'nope_i_am_not_sure');
                    this._showMenu(this._initialMenu());
                },
                tooltip: ''
            },
        ];

        return items;
    },

    _helpMenu() {
        if (typeof ga === "function") ga('send', 'event', 'menu_navigation', 'help');
        this.help.visible = true;
        let items = [{
            text: "Back",
            action: () => {
                this._showMenu(this._initialMenu());
            },
            tooltip: ''
        }];
        return items;
    },

    _levelSelectMenu() {
        if (typeof ga === "function") ga('send', 'event', 'menu_navigation', 'level_list');
        this.help.visible = false;
        let items = [];

        for (let i = 0; i < CAMPAIGN.length; i++) {
            let item = {
                text: `${CAMPAIGN[i].startsWith('Level') ? CAMPAIGN[i].substring(5, CAMPAIGN[i].length) : CAMPAIGN[i]}`,
                action: () => {
                    game.storage.saveLevelIndex(i);
                    game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
                        if (typeof ga === "function") ga('send', 'event', 'menu_navigation', 'level_select', CAMPAIGN[i]);
                        game.state.start("level_state");
                    });
                },
                tooltip: '',
                devItem: true
            };
            items.push(item);
        }
        items.push({
            text: "Back",
            action: () => {
                this._showMenu(this._initialMenu());
            },
            tooltip: ''
        });

        return items;
    },


    update() {

        if (this.cursors.up.justDown) {
            this.selectedItem = (this.items.length + this.selectedItem - 1) % this.items.length;

        } else if (this.cursors.down.justDown) {
            this.selectedItem = (this.selectedItem + 1) % this.items.length;

        }
        this.pointer.y = this.labels[0].y + this.selectedItem * ITEMS_PADDING;
        this.tooltip.text = this.items[this.selectedItem].tooltip;

        if (this.keys.enter.justDown || this.keys.space.justDown) {
            this.items[this.selectedItem].action();
            game.sounder.play('click_1');
        }
    },

    shutdown() {
        for (let label of this.labels) label.destroy();
        // this.title.destroy();
        this.pointer.destroy();
        this._theme.destroy();
    }
};
