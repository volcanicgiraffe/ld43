
const Walls = {
    [`www
      wWw
      ww.`]: 1,
    [`www
      wWw
      ...`]: 2,
    [`www
      wWw
      ..w`]: 2,
    [`www
      wWw
      w..`]: 2,
    [`www
      wWw
      .ww`]: 3,
    [`www
      wWw
      .w.`]: 4,
    [`.w.
      wWw
      www`]: 5,
    [`.ww
      wWw
      .ww`]: 6,
    [`ww.
      wWw
      ww.`]: 7,

    [`ww.
      wW.
      ww.`]: 9,
    [`www
      wW.
      ww.`]: 9,
    [`ww.
      wW.
      www`]: 9,
    [`www
      wWw
      www`]: 10,
    [`.ww
      .Ww
      .ww`]: 11,
    [`www
      .Ww
      .ww`]: 11,
    [`.ww
      .Ww
      www`]: 11,
    [`...
      .Ww
      .ww`]: 12,
    [`...
      wW.
      ww.`]: 13,
    [`.w.
      .W.
      .w.`]: 14,
    [`...
      wWw
      ...`]: 15,
    [`...
      .W.
      .w.`]: 16,
    [`ww.
      wWw
      www`]: 17,
    [`...
      wWw
      www`]: 18,
    [`..w
      wWw
      www`]: 18,
    [`w..
      wWw
      www`]: 18,
    [`.ww
      wWw
      www`]: 19,
    [`.ww
      .Ww
      ...`]: 20,
    [`ww,
      wW.
      ...`]: 21,
    [`.w.
      .W.
      .W.`]: 22,
    [`...
      .Ww
      ...`]: 23,
    [`...
      wW.
      ...`]: 24,


    //here floor

    [`www
      ...
      ...`]: 26,
    [`ww.
      ...
      ...`]: 26,
    [`.ww
      ...
      ...`]: 26,
    [`.w.
      ...
      ...`]: 26,
    [`..w
      ..w
      ..w`]: 27,
    [`...
      ..w
      ..w`]: 27,
    [`..w
      ..w
      ...`]: 27,
    [`...
      ..w
      ...`]: 27,
    [`w..
      w..
      w..`]: 28,
    [`...
      w..
      w..`]: 28,
    [`w..
      w..
      ...`]: 28,
    [`...
      w..
      ...`]: 28,
    [`...
      ...
      www`]: 29,
    [`...
      ...
      ww.`]: 29,
    [`...
      ...
      .ww`]: 29,
    [`...
      ...
      .w.`]: 29,
    [`..w
      ..w
      www`]: 30,
    [`...
      ..w
      www`]: 30,
    [`..w
      ..w
      .ww`]: 30,
    [`...
      ..w
      .ww`]: 30,

    [`w..
      w..
      www`]: 31,
    [`...
      w..
      www`]: 31,
    [`w..
      w..
      ww.`]: 31,
    [`...
      w..
      ww.`]: 31,

    [`www
      ..w
      ..w`]: 32,
    [`.ww
      ..w
      ..w`]: 32,
    [`www
      ..w
      ...`]: 32,
    [`.ww
      ..w
      ...`]: 32,

    [`www
      w..
      w..`]: 33,
    [`www
      w..
      ...`]: 33,
    [`ww.
      w..
      w..`]: 33,
    [`ww.
      w..
      ...`]: 33,

    [`...
      ...
      ..w`]: 39,
    [`...
      ...
      w..`]: 40,
    [`..w
      ...
      ...`]: 47,
    [`w..
      ...
      ...`]: 48,

};


function template2fn(template) {
    let rows = template.split("\n").map(c => c.trim()).filter(c => c.length);
    return function check(x, y, isWall) {
        for (let dx = -1; dx <= 1; dx++) {
            for (let dy = -1; dy <= 1; dy++) {
                let reqWall = rows[dy+1].charAt(dx+1).toLowerCase() === 'w';
                if (reqWall !== isWall(x+dx, y+dy)) return false;
            }
        }
        return true;
    }
}

export default class WallPatcher {

    constructor(tilemap) {
        this.tilemap = tilemap;

        this.templateFunctions = [];
        for (let template in Walls) {
            this.templateFunctions.push({
                check: template2fn(template),
                tileNr: Walls[template]
            })
        }
    }

    replaceTile(x, y, layer) {
        for (let {check, tileNr} of this.templateFunctions) {
            if (check(x, y, this.isWall.bind(this))) {
                this.tilemap.putTile(tileNr, x, y, layer);
                return;
            }
        }
    }

    isWall(x, y) {
        if (x < 0 || y < 0 || x >= this.tilemap.width || y >= this.tilemap.height) return true;
        let tile = this.tilemap.getTile(x, y, "walls");
        return !!tile;
    }

    patch() {
        for (let x = 0; x < this.tilemap.width; x++) {
            for (let y = 0; y < this.tilemap.height; y++) {
                let wallTile = this.tilemap.getTile(x, y, "walls");
                if (wallTile && wallTile.index === 10) {
                    this.replaceTile(x, y, "walls");
                }
                let floorTile = this.tilemap.getTile(x, y, "floor");
                if (floorTile && floorTile.index === 25) {
                    this.replaceTile(x, y, "floor");
                }
            }
        }
    }

}