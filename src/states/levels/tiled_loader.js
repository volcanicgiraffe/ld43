import Door from "../../objects/environment/door";
import Monster from "../../objects/monsters/monster";
import Unit from "../../objects/units/unit";
import Fan from "../../objects/environment/fan";
import PressButton from "../../objects/environment/button";
import ExitZone from "../../objects/environment/exit_zone";
import CameraMover from "../../objects/environment/camera_mover";
import { PIX } from "../../consts";
import Terminal from "../../objects/environment/terminal";
import WallPatcher from "./wall_patcher";
import Decal from "../../objects/environment/decal";
import ExitGate from "../../objects/environment/exitGate";
import UnitAsLoot from "../../objects/units/unit_as_loot";
import Sign from "../../objects/environment/sign";
import Light from "../../effects/lights/Light";

export default class TiledLoader {

    constructor(name) {
        this.tilemap = game.add.tilemap(name);
        this.tilemap.addTilesetImage('ld43_tiles', 'tiles');

        this.lettersPerId = new Map();
        this.nonUsedLetters = ['a', 'b', 'd', 'e', 'f', 'g', 'h', 'i'];
    }

    loadTilemap(levelState) {

        this.levelState = levelState;

        new WallPatcher(this.tilemap).patch();

        this._fillFloors();
        this._fillDecals();
        this._fillWalls();
        this._fillObjects('objects');
        this._fillObjects('buttons'); // we need to load buttons after the objects due to dependency
        this._fillObjects('lights');
    }

    _fillWalls() {
        let layer = this.tilemap.createLayer("walls");
        this.levelState.wallsGrp.add(layer);
        layer.resizeWorld();

        this.tilemap.forEach((tile) => {
            switch (tile.index) {
                case -1:
                case 0:
                    // do nothing
                    break;
                default:
                    this.levelState.passMap.setWallSprite({ x: tile.x * PIX, y: tile.y * PIX, width: PIX, height: PIX });
            }

        }, null, 0, 0, this.width, this.height, this.tilemap.getLayer('walls'));
    }

    _fillFloors() {
        let layer = this.tilemap.createLayer("floor");
        this.levelState.floorGrp.add(layer);
        layer.resizeWorld();
    }

    _fillDecals() {
        if (!this.tilemap.objects.decals) return;
        this.tilemap.objects.decals.forEach((d) => {
            this.levelState.decalGrp.add(new Decal(d.x, d.y, d.name));
        });
    }


    _fillObjects(layer) {
        if (!this.tilemap.objects[layer]) return;

        for (let obj of this.tilemap.objects[layer]) {
            let { x, y, name, type, properties, width, height, rectangle, polyline } = obj;
            properties = properties || {};

            let targetIds = properties['target-id'] && properties['target-id'].split(',') || [];
            let objectId = name || 'default';

            // name as ID.

            switch (type) {
                case 'Door':
                    this.levelState.envGrp.add(new Door(x, y, width, height, objectId,
                        properties['opened'], properties['strong'], properties['health'], properties['inverse']));
                    break;
                case 'Unit':
                    this.levelState.unitsGrp.add(new Unit(x + width / 2, y + height / 2, {}));
                    break;
                case 'UnitAsLoot':
                    this.levelState.decalGrp.add(new UnitAsLoot(x + width / 2, y + height / 2));
                    break;
                case 'UnitSpawner':
                    this.levelState.spawnUnits(new Phaser.Rectangle(x, y, width, height));
                    break;
                case 'Monster':
                    this.levelState.monsterGrp.add(new Monster(x + width / 2, y + height / 2, {}));
                    break;
                case 'Fan':
                    this.levelState.envGrp.add(new Fan(x, y, objectId, properties['enabled']));
                    break;
                case 'PressButton':
                    let button = new PressButton(x, y, targetIds)
                    this.levelState.envGrp.add(button);
                    if (properties['letter']) {
                        let sign = new Sign(x, y, button.width, button.height, properties['letter'], undefined, true);
                        this.levelState.envGrp.add(sign._letter);
                    }
                    break;
                case 'ExitGate':
                    this.levelState.envGrp.add(new ExitGate(x, y, properties['orientation']));
                    break;
                case 'Terminal':
                    const trm = new Terminal(x, y, targetIds, properties['orientation']);
                    this.levelState.envGrp.add(trm);
                    break;
                case 'ExitZone':
                    let exitZone = new ExitZone(x, y, width, height, objectId);
                    this.levelState.envGrp.add(exitZone);
                    this.levelState.exitZone = exitZone; // could be an array though...
                    break;
                case 'CameraMover':
                    this.levelState.cameraMover = new CameraMover(x, y);
                    break;
                case 'Light':
                    let light = new Light(x, y, width, height, name, properties);
                    //nothing to do, it registers itself
                    break;
                case 'Sign':
                    //let targetId = targetIds[0] || name;
                    let letter = properties['letter'] || name;//this.lettersPerId.get(targetId);
                    if (!letter) {
                        console.error("define letter for sign! in props or name");
                    }
                    this.nonUsedLetters = this.nonUsedLetters.filter(l => l !== letter);
                    let sign = new Sign(x, y, width, height, letter);
                    this.levelState.envGrp.add(sign);
                    this.levelState.envGrp.add(sign._letter);
                    break;
                default:
                    // console.log('Tiled: Ignored object', type, x, y, name)
            }
        }
    }

    get width() {
        return this.tilemap.width;
    }

    get height() {
        return this.tilemap.height;
    }
}
