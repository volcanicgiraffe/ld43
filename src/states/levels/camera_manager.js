import { PIX } from "../../consts";
import CameraMover from "../../objects/environment/camera_mover";

const ZOOM_AMNT = 0.05;
const CAMERA_KB_SPEED = 5;

const MOUSE_ZONE = 50;
const UI_H = 0; //UI

export default class CameraManager {
    constructor() {
        this.cameraMover = game.level.cameraMover || new CameraMover(game.world.width / 2, game.world.height / 2);

        game.level.serviceGroup.add(this.cameraMover);
        this.follow(this.cameraMover);

        // game.input.mouse.mouseWheelCallback = (e) => {
        //     if (e.wheelDelta < 0) {
        //         this.zoomOut();
        //     } else {
        //         this.zoomIn();
        //     }
        // };
    }

    adjust() {
        let gww = game.world.width;
        let gwh = game.world.height;
        let wiw = window.innerWidth;
        let wih = window.innerHeight;
        let uih = 200;

        if (gww > wiw || gwh + uih > wih) {
            let camW = gww > wiw ? gww + PIX : wiw;
            let camH = gwh + uih > wih ? gwh + uih : wih;

            game.camera.bounds = new Phaser.Rectangle(0, 0, camW, camH);
            if (this.target != null) game.camera.follow(this.target, undefined, 0.1, 0.1);
        } else {
            game.camera.bounds = null;
            game.camera.unfollow();
            game.camera.setPosition(0, 0);
        }
    }

    follow(target) {
        this.target = target;
        this.adjust();
    }

    zoomIn() {
        game.camera.scale.x = Math.min(1, game.camera.scale.x + ZOOM_AMNT);
        game.camera.scale.y = Math.min(1, game.camera.scale.y + ZOOM_AMNT);
    }

    zoomOut() {
        game.camera.scale.x = Math.max(0.25, game.camera.scale.x - ZOOM_AMNT);
        game.camera.scale.y = Math.max(0.25, game.camera.scale.y - ZOOM_AMNT);
    }

    unfollow() {
        this.target = null;
        game.camera.unfollow();
    }

    update() {
        let halfW = game.world.width / 2;
        let halfH = game.world.height / 2;

        let limitLeft = game.camera.atLimit.x && this.cameraMover.x <= halfW;
        let limitRight = game.camera.atLimit.x && this.cameraMover.x >= halfW;
        let limitTop = game.camera.atLimit.y && this.cameraMover.y <= halfH;
        let limitBottom = game.camera.atLimit.y && this.cameraMover.y >= halfH;

        let movedByKb = false;

        if (game.anyInput.holdLeft() && !limitLeft) {
            this.cameraMover.x -= CAMERA_KB_SPEED;
            movedByKb = true;
        }
        if (game.anyInput.holdRight() && !limitRight) {
            this.cameraMover.x += CAMERA_KB_SPEED;
            movedByKb = true;
        }
        if (game.anyInput.holdUp() && !limitTop) {
            this.cameraMover.y -= CAMERA_KB_SPEED;
            movedByKb = true;
        }
        if (game.anyInput.holdDown() && !limitBottom) {
            this.cameraMover.y += CAMERA_KB_SPEED;
            movedByKb = true;
        }

        let mx = game.input.activePointer.x;
        let my = game.input.activePointer.y;

        let camRect = game.camera.view;

        if (movedByKb || (this._oldMx === mx && this._oldMy === my)) {
            this._oldMx = mx;
            this._oldMy = my;
            return;
        }

        if (mx >= 0 && mx <= MOUSE_ZONE && !limitLeft) {
            this.cameraMover.x -= CAMERA_KB_SPEED * (mx < MOUSE_ZONE / 2 ? 2 : 1);
        }
        if (mx >= camRect.width - MOUSE_ZONE && mx <= camRect.width && !limitRight) {
            this.cameraMover.x += CAMERA_KB_SPEED * (mx > camRect.width - MOUSE_ZONE / 2 ? 2 : 1);
        }

        if (my >= 0 && my <= MOUSE_ZONE && !limitTop) {
            this.cameraMover.y -= CAMERA_KB_SPEED * (my < MOUSE_ZONE / 2 ? 2 : 1);
        }

        if (my > camRect.height - UI_H && mx > 490 && mx < 573) return; // ignore UI;

        if (my >= camRect.height - UI_H - MOUSE_ZONE && (my <= camRect.height) && !limitBottom) {
            this.cameraMover.y += CAMERA_KB_SPEED * (my > camRect.height - MOUSE_ZONE / 2 ? 2 : 1);
        }
    }
}
