import { initState, StateBase } from "../../utils/state";
import Unit, {STATE_NORMAL} from "../../objects/units/unit";
import PassMap, { PASS_CELL_SIZE } from "./passmap";
import TiledLoader from "./tiled_loader";
import CameraManager from "./camera_manager";
import { CAMPAIGN, PIX } from "../../consts";
import NoiseManager from "../../objects/sound/NoiseManager";
import NoiseRenderer from "../../objects/sound/NoiseRenderer";
import GameEnder from "./game_ender";
import Atmosphere, {BIG_LIGHT_RADIUS} from "../../effects/Atmosphere";
import MonsterHider from "../../effects/MonsterHider";
import SelectedPanel from "../../objects/ui/Selected";
import ExitHelper from "../../objects/ui/ExitHelper";
import ClickPointer from "../../objects/environment/click_pointer";
import Light from "../../effects/lights/Light";
import LightTexture from "../../effects/lights/LightTexture";

class UIState extends StateBase {
    constructor() {
        super("ui");

        this.selectedUnits = [];

        this.input = {
            leftButtonDown: false,
            rightButtonDown: false,
            dblClick: false
        };

        this.selectionBox = {
            x: 0,
            y: 0,
            x2: 0, y2: 0,
            visible: false
        };

        this.postConstruct();
    }
}

class GlobalState extends StateBase {
    constructor() {
        super("global");

        this.activeUnitsCountChanged = false;
        this.openedDoorsChanged = false;
        this.anybodyMoved = false;
        this.anyStatusChanged = false;

        this.evacuatedUnits = [];
        this.unitsInExitZoneCount = [];

        this.postConstruct();
    }
}

const DEBUG_PASSING = false;
const DEBUG_NOISE = false;
const DEBUG_HUNT_TARGET = false;
const DEBUG_STATE = false;

const DEBUG_LIGHT = false;
const DEBUG_MONSTER = false;

export default {
    init(options) {
        this.profilesToSpawn = game.storage.loadProfiles();
        this.levelName = CAMPAIGN[game.storage.loadLevelIndex() % CAMPAIGN.length];

        if (typeof ga === "function") ga('send', 'event', 'state', this.levelName);

        game.DEBUG_STATE = DEBUG_STATE;
        game.DEBUG_LIGHT = DEBUG_LIGHT;

        //game.world.scale.set(0.5);
        //game.scale.setUserScale(0.5, 0.5);
        //game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;

        this._theme = game.add.sound('aracnho-1');
        this._theme.play(undefined, undefined, 0, true);
        this._theme.volume = 0.8;

    },

    create() {
        game.add.tween(game.world).to({ alpha: 1 }, 100, null, true);

        this.deadPeople = [];
        this.levelTimer = 0;

        this.keys = {
            one: game.input.keyboard.addKey(Phaser.KeyCode.ONE),
            two: game.input.keyboard.addKey(Phaser.KeyCode.TWO),
            three: game.input.keyboard.addKey(Phaser.KeyCode.THREE)
        };

        this.serviceGroup = game.add.group(undefined, 'service');
        this.floorGrp = game.add.group(undefined, "floor");
        this.decalGrp = game.add.group(undefined, "decals");
        this.wallsGrp = game.add.group(undefined, "walls");
        this.envGrp = game.add.group(undefined, "environment");
        this.lightsGrp = game.add.group(undefined, "lights");
        this.shadowsGrp = game.add.group(undefined, "shadows");
        this.unitsGrp = game.add.group(undefined, "units");
        this.monsterGrp = this.monsterHider = new MonsterHider();//game.add.group(undefined, "monsters");
        this.noiseGrp = game.add.group(undefined, "noise");
        this.selectionGrp = game.add.group(undefined, "selection");
        this.uiGrp = game.add.group(undefined, "ui");
        this.state = initState(this);
        this.state.ui = new UIState();
        this.state.global = new GlobalState();

        this.pauseText = game.add.bitmapText(game.canvas.width / 2, game.canvas.height / 2 - 20, "font36", "Go back to main menu? ", 36);
        let pauseHint = game.add.bitmapText(0, 40, "font36", "Yes [ENTER] | Nope [ESC]", 36);
        this.pauseText.addChild(pauseHint);
        this.pauseText.tint = 0xFFFF00;
        pauseHint.tint = 0xFFFF00;
        pauseHint.anchor.set(0.5);
        this.pauseText.anchor.set(0.5);
        this.pauseText.visible = false;

        this.uiGrp.add(this.pauseText);

        this.tiledLoader = new TiledLoader(this.levelName);
        this.noise = new NoiseManager();
        this.passMap = new PassMap(this.tiledLoader.width * PIX, this.tiledLoader.height * PIX);
        this.lightTexture = new LightTexture(this.lightsGrp, this.shadowsGrp, this.tiledLoader.width * PIX, this.tiledLoader.height * PIX);

        this.tiledLoader.loadTilemap(this);
        if (DEBUG_NOISE) this.noiseRenderer = new NoiseRenderer();

        this.selectionBox = game.add.spriteFilled(0, 0, 1, 1, "rgba(0,255,0,0.1)");
        this.selectionBox.visible = false;
        this.selectionGrp.add(this.selectionBox);

        this.cameraManager = new CameraManager();
        this.gameEnder = new GameEnder();

        this.state.global.onEvent("activeUnitsCountChanged", this.updateSelectionIds.bind(this));
        this.state.global.onEvent("openedDoorsChanged", this.resetPaths.bind(this));

        this.initUI();

        game.input.onTap.add((pointer, isDblClick) => {
           this.state.ui.input.dblClick = isDblClick;
        });

        this._clickPointer = new ClickPointer();
        this.decalGrp.add(this._clickPointer);

        if (game.DEBUG_LIGHT) {
            this.lightTexture.debug();
        }
        this.lightTexture.init();
        if (!DEBUG_MONSTER) this.world.filters = [this.atmosphere = new Atmosphere()];

        this.completed = false;
        game.sounder.resetAll();
    },

    initUI() {
        this.uiGrp.fixedToCamera = true;
        this.uiGrp.add(new SelectedPanel());
        this.uiGrp.add(new ExitHelper());


        this.uiMask = game.add.renderTexture(game.camera.width, game.camera.height);
        this.uiMask.sprite = game.add.sprite(-1000, -1000, this.uiMask);
        this.uiMask.render(this.uiGrp);
    },

    resetPaths() {
        for (let unit of this.unitsGrp.children) {
            Unit.unitResetPath(unit.state);
        }
        for (let monster of this.monsterGrp.children) {
            Unit.unitResetPath(monster.state);
        }
    },

    updateSelectionIds() {
        this.state.ui.selectedUnits = this.state.ui.selectedUnits.filter(id => Unit.canBeSelected(this.state.objects[id]));
    },

    update() {
        if (this.completed) return; // game end

        this.levelTimer += game.time.physicsElapsedMS;

        this.lightTexture.update();
        this.monsterHider.update();
        this.state.global.checkChanges();
        this.state.ui.checkChanges();
        this.updateKbInput();
        this.updateSelection();
        this.updateOrders();
        this.checkMonsterOverlap();
        this.checkButtonOverlap();
        this.updateNeighbours();
        //this.showAlerts();
        this.cameraManager.update();

        this.selectionBox.visible = this.state.ui.selectionBox.visible;
        this.selectionBox.x = Math.min(this.state.ui.selectionBox.x, this.state.ui.selectionBox.x2);
        this.selectionBox.y = Math.min(this.state.ui.selectionBox.y, this.state.ui.selectionBox.y2);
        this.selectionBox.width = Math.abs(this.state.ui.selectionBox.x2 - this.state.ui.selectionBox.x);
        this.selectionBox.height = Math.abs(this.state.ui.selectionBox.y2 - this.state.ui.selectionBox.y);


        if (DEBUG_PASSING && this.keys.one.justDown) {
            this.debugUnitPath();
        }

        this.noise.afterUpdate();

        this.gameEnder.update();
    },

    postUpdate() {
        if (!DEBUG_MONSTER) this.atmosphere.update();
    },


    updateNeighbours() {
        for (let ui = 0; ui < this.unitsGrp.children.length; ui++) {
            this.unitsGrp.children[ui].state.neighbours = 0;
        }
        for (let ui = 0; ui < this.unitsGrp.children.length; ui++) {
            for (let ui2 = ui + 1; ui2 < this.unitsGrp.children.length; ui2++) {
                let u1 = this.unitsGrp.children[ui];
                let u2 = this.unitsGrp.children[ui2];
                if (Phaser.Point.distance(u1, u2) < 128) { //todo: const
                    u1.state.neighbours++;
                    u2.state.neighbours++;
                }
            }
        }
    },

    showAlerts() {
        for (let m of this.monsterGrp.children) {
            if (m.state.substate === "hunting" && m.state.huntingTarget) {
                // I cant into architecture right now sorry
                let newTarget = false;
                if (!m.state.lastTarget) {
                    newTarget = true;
                } else if (m.state.huntingTarget.sprite) {
                    newTarget = m.state.huntingTarget.sprite !== m.state.lastTarget.sprite;
                } else {
                    newTarget = m.state.huntingTarget !== m.state.lastTarget
                }
                if (newTarget) {
                    let target = m.state.huntingTarget.noise ? m.state.huntingTarget.noise : m.state.huntingTarget;

                    m.state.lastTarget = m.state.huntingTarget;
                }
            }
        }

    },

    checkButtonOverlap() {
        for (let c of this.envGrp.children) {
            if (c.canBePressed) {
                c.setPressed(this.unitsGrp.children.some(u => u.overlap(c)));
            } else if (c.interactive) {
                // TODO should be only enabled when close to person who is tasked to enable it.
                if (this.unitsGrp.children.some(u => u.overlap(c))) {
                    c.enable(true)
                }
            }
        }
    },

    updateKbInput() {
        if (this.completed) return;

        if (game.anyInput.pressedRestart()) {
            this.restartLevel();
        }

        if (game.anyInput.pressedEvacuate() && this.pauseText.visible) {
            game.sounder.play('click_1');
            this.pauseText.visible = false;
            this.completed = true;
            game.state.start("menu");
            game.world.removeAll(true, false);
        }

        if (game.anyInput.pressedMenu()) {
            game.sounder.play('click_1');
            this.pauseText.visible = !this.pauseText.visible;
        }
    },

    updateSelection() {
        let uiState = this.state.ui;
        let pointer = game.input.activePointer;
        uiState.input.leftButtonDown = pointer.leftButton.isDown;
        uiState.input.rightButtonDown = pointer.rightButton.isDown;

        if (uiState.selectionBox.visible) {
            if (uiState.input.leftButtonDown) {
                uiState.selectionBox.x2 = pointer.worldX;
                uiState.selectionBox.y2 = pointer.worldY;
            } else {
                uiState.selectionBox.visible = false;
                let rectangle = new Phaser.Rectangle(
                    Math.min(uiState.selectionBox.x, uiState.selectionBox.x2),
                    Math.min(uiState.selectionBox.y, uiState.selectionBox.y2),
                    Math.abs(uiState.selectionBox.x2 - uiState.selectionBox.x),
                    Math.abs(uiState.selectionBox.y2 - uiState.selectionBox.y));
                if (rectangle.width + rectangle.height > 4 || pointer.y < this.uiGrp.children[0].top) {
                    uiState.selectedUnits = [];
                    for (let unit of this.unitsGrp.children) {
                        if (Phaser.Rectangle.contains(rectangle, unit.x, unit.y)) {
                            uiState.selectedUnits.push(unit.state.id);
                        }
                    }
                }
            }
        } else {

            if (uiState.input.dblClick) {
                uiState.input.dblClick = false;
                let selected = false;
                for (let unit of this.unitsGrp.children) {
                    if (Phaser.Rectangle.containsPoint(unit.getBounds(), pointer)) {
                        uiState.selectedUnits = [unit.state.id];
                        selected = true;
                    }
                }
                if (selected) {
                    //select all idle
                    uiState.selectedUnits = this.state.objects.unit.filter(u => u.mainState === STATE_NORMAL && u.substate === "idle").map(u => u.id);
                }
            } else if (uiState.input.leftButtonDown) {
                //uiState.selectedUnits = [];
                let selected = false;
                for (let unit of this.unitsGrp.children) {
                    if (Phaser.Rectangle.containsPoint(unit.getBounds(), pointer)) {
                        uiState.selectedUnits = [unit.state.id];
                        selected = true;
                    }
                }

                if (!selected) {
                    uiState.selectionBox.visible = true;
                    uiState.selectionBox.x = uiState.selectionBox.x2 = pointer.worldX;
                    uiState.selectionBox.y = uiState.selectionBox.xy = pointer.worldY;
                }
            }
        }


        uiState.ifChanged(n => n.selectedUnits, () => {
            for (let unit of this.unitsGrp.children) {
                unit.setSelected(uiState.selectedUnits.includes(unit.state.id));
            }

        });

    },

    updateOrders() {
        let uiState = this.state.ui;
        let pointer = game.input.activePointer;

        if (uiState.selectedUnits) {
            uiState.ifChanged(n => n.input.rightButtonDown, () => {
                if (pointer.rightButton.isDown) return;

                let targetPoint = this._findClosestPassable(pointer.worldX, pointer.worldY);

                for (let id of uiState.selectedUnits) {
                    if (Unit.canExecuteOrder(this.state.objects[id])) {
                        Unit.unitMoveTo(this.state.objects[id], targetPoint);

                        this._clickPointer.onClick(targetPoint);
                        game.sounder.play('click');
                    }
                }
            });

        }

    },

    _findClosestPassable(x, y) {
        if (!this.passMap.cellIsBlockedAtPoint(x, y)) return { x, y };

        let radius = PIX;

        let neighbours = [
            { dx: 1, dy: 0 }, { dx: -1, dy: 0 }, { dx: 0, dy: 1 }, { dx: 0, dy: -1 },
            { dx: -1, dy: -1 }, { dx: 1, dy: -1 }, { dx: 1, dy: 1 }, { dx: -1, dy: 1 }
        ];
        for (let { dx, dy } of neighbours) {
            if (!this.passMap.cellIsBlockedAtPoint(x + dx * radius, y + dy * radius)) {
                return { x: x + dx * radius, y: y + dy * radius };
            }
        }
        return { x, y }; // not found, so this point is blocked
    },

    checkMonsterOverlap() {
        for (let unit of this.unitsGrp.children) {
            if (!unit.canBeGrabbed) continue;
            let sees = false;
            for (let monster of this.monsterGrp.children) {
                //I DO NOT UNDERSTAND WHY NO MORE WORK
                if (monster.canGrab(unit)) {
                    monster.eatUnit(unit);
                    sees = true;
                } else if (!this.passMap.isSomethingBetween(monster.x, monster.y, unit.x, unit.y)) {
                    monster.sees(unit);
                    if (Phaser.Point.distance(monster, unit) < BIG_LIGHT_RADIUS) sees = true;
                }
            }
            Unit.unitSeesMonster(unit.state, sees);
        }
    },

    restartLevel() {
        if (typeof ga === "function") ga('send', 'event', 'game_end', game.level.levelName, 'restart');
        this.gameEnder.levelFail();
    },

    render() {
        // game.debug.text('FPS: ' + (game.time.fps || '--'), 4, game.canvas.height - 24);
        if (DEBUG_PASSING) this.debugPassMap();
        if (DEBUG_HUNT_TARGET) this.debugHuntTarget();
        //game.debug.cameraInfo(game.camera, 32, game.canvas.height - 150);

        /*if (game.camera.deadzone) {
            var zone = game.camera.deadzone;

            game.context.fillStyle = 'rgba(255,0,0,0.6)';
            game.context.fillRect(zone.x, zone.y, zone.width, zone.height);
        }*/
        if (DEBUG_MONSTER) {
            for (let m of this.monsterGrp.children) {
                if (m.state.substate === "hunting" && m.state.huntingTarget) {
                    game.debug.geom(new Phaser.Circle(m.state.x, m.state.y, 24),  "rgba(255,0,0,0.8)");
                }
            }

        }
    },

    debugHuntTarget() {
        for (let m of this.monsterGrp.children) {
            if (m.state.substate === "hunting" && m.state.huntingTarget) {
                game.debug.geom(new Phaser.Circle(m.state.huntingTarget.x, m.state.huntingTarget.y, 24), m.state.huntingTarget.noise ? "rgba(255,200,0,0.4)" : "rgba(255,0,128,0.4)");
            }
        }
    },

    debugPassMap() {
        if (!this.debugPMBitmap) {
            this.debugPMBitmap = game.add.bitmapData(this.passMap.width, this.passMap.height);
            this.debugPMBitmap.sprite = game.add.sprite(0, 0, this.debugPMBitmap);
            this.debugPMBitmap.sprite.scale.set(PASS_CELL_SIZE);
            this.debugPMBitmap.sprite.alpha = 0.1;
        }
        this.debugPMBitmap.update();
        for (let x = 0; x < this.passMap.width; x++) {
            for (let y = 0; y < this.passMap.height; y++) {
                let cell = this.passMap.getCell(x, y);
                if (cell.wall) {
                    this.debugPMBitmap.setPixel(x, y, 40, 40, 200, false);
                } else if (cell.door && !cell.opened) {
                    this.debugPMBitmap.setPixel(x, y, 40, 0, 0, false);
                } else if (cell.nearWall) {
                    this.debugPMBitmap.setPixel(x, y, 120, 60, 60, false);
                } else {
                    this.debugPMBitmap.setPixel32(x, y, 0, 0, 0, 0, false);
                }
            }
            this.debugPMBitmap.context.putImageData(this.debugPMBitmap.imageData, 0, 0);
            this.debugPMBitmap.dirty = true;

        }
    },


    debugUnitPath() {
        if (this.state.ui.selectedUnits.length) {
            let sp = this.state.sprites[this.state.ui.selectedUnits[0]];
            let path = this.passMap.createUnitFinder()(sp.x, sp.y, game.input.activePointer.worldX, game.input.activePointer.worldY);
            if (!this.debugPathBitmap) {
                this.debugPathBitmap = game.add.bitmapData(game.world.width, game.world.height);
                this.debugPathBitmap.sprite = game.add.sprite(0, 0, this.debugPathBitmap);
            }
            this.debugPathBitmap.clear();
            this.debugPathBitmap.ctx.strokeStyle = "purple";
            this.debugPathBitmap.ctx.beginPath();
            if (path.length) {
                this.debugPathBitmap.ctx.moveTo(sp.x, sp.y);
                for (let i = 0; i < path.length; i++) {
                    this.debugPathBitmap.ctx.lineTo(path[i].x, path[i].y);
                }
            } else {
                this.debugPathBitmap.ctx.strokeText("No path :(", sp.x, sp.y);
            }
            this.debugPathBitmap.ctx.stroke();
            this.debugPathBitmap.dirty = true;
        }
    },

    spawnUnits(rectangle) {
        for (let profile of this.profilesToSpawn) {
            let rx = game.rnd.integerInRange(rectangle.x, rectangle.x + rectangle.width);
            let ry = game.rnd.integerInRange(rectangle.y, rectangle.y + rectangle.height);

            this.unitsGrp.add(new Unit(rx, ry, profile));
        }
    },

    get width() {
        return this.tiledLoader.width;
    },

    get height() {
        return this.tiledLoader.height;
    },


    shutdown() {
        this._theme.destroy();
    }
};
