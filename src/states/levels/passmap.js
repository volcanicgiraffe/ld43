import createGraph from 'ngraph.graph'
import path from 'ngraph.path'
import { PIX } from "../../consts";

export const PASS_CELL_SIZE = 16;//PIX;

export default class PassMap {

    //this map contains big grid with passable/unpassable cells
    constructor(widthPix, heightPix) {

        this.width = widthPix/PASS_CELL_SIZE;
        this.height = heightPix/PASS_CELL_SIZE;

        this.grid = [];
        for (let x = 0; x < this.width; x++) {
            let col = [];
            for (let y = 0; y < this.height; y++) {
                col.push({x, y, id: x + '_' + y});
            }
            this.grid.push(col);
        }
    }


    getCell(gx, gy) {
        if (gx < 0 || gy < 0 || gx >= this.width || gy >= this.height) return null;
        return this.grid[gx][gy];
    }

    getCellForPoint(x, y) {
        let gx = (x/PASS_CELL_SIZE)|0;
        let gy = (y/PASS_CELL_SIZE)|0;
        return this.getCell(gx, gy);
    }

    cellIsBlockedAtPoint(x, y) {
        let cell = this.getCellForPoint(x, y);
        return cell == null || cell.nearWall || cell.wall || cell.passable === false;
    }

    setWallSprite(w) {
        this.forAnyCellUnderSprite(w, cell => {
            cell.wall = true;
            this.forAnyCell(cell.x-1, cell.y-1, cell.x+1, cell.y+1, cell => {
                cell.nearWall = true;
                cell.passable = false;
                cell.passableForMonster = false;
            });
        });
    }

    forAnyCellUnderSprite({x, y, width, height}, cb) {
        let gx1 = (x/PASS_CELL_SIZE)|0;
        let gy1 = (y/PASS_CELL_SIZE)|0;
        let gx2 = ((x+width-1)/PASS_CELL_SIZE)|0;
        let gy2 = ((y+height-1)/PASS_CELL_SIZE)|0;
        for (let x = gx1; x <= gx2; x++) {
            for (let y = gy1; y <= gy2; y++) {
                let cell = this.getCell(x, y);
                if (cell) {
                    cb(cell);
                }
            }
        }
    }

    forAnyCell(gx1, gy1, gx2, gy2, cb) {
        for (let x = gx1; x <= gx2; x++) {
            for (let y = gy1; y <= gy2; y++) {
                let cell = this.getCell(x, y);
                if (cell) {
                    cb(cell);
                }
            }
        }
    }

    setDoorSprite(door, strong) {
        let doorCells = [];
        this.forAnyCellUnderSprite(door.colBox, cell => {
            doorCells.push(cell);
            cell.doorSprite = door;
            cell.door = true;
            cell.opened = door.state.opened;
            cell.passable = door.state.opened;
            cell.strongDoor = true;
            if (strong) {
                cell.passableForMonster = door.state.opened;
            }
        });
        door.state.onChange(n => n.opened, newVal => {
            for (let dc of doorCells) {
                dc.opened = newVal;
                dc.passable = newVal;
                if (strong) {
                    dc.passableForMonster = newVal;
                }
            }
        })
    }

    setWall(gx, gy) {
        let cell = this.getCell(gx, gy);
        if (!cell) return;
        cell.wall = true;
    }

    setDoor(gx, gy, opened = true) {
        let cell = this.getCell(gx, gy);
        if (!cell) return;
        cell.door = true;
        cell.opened = opened;
        cell.passable = opened;
    }

    setTag(gx, gy, tag) {
        let cell = this.getCell(gx, gy);
        cell[tag] = true;
    }


    findUnitPath(fromX, fromY, toX, toY) {
        return this.createUnitFinder()(fromX, fromY, toX, toY);
    }

    findMonsterPath(fromX, fromY, toX, toY) {
        return this.createMonsterFinder()(fromX, fromY, toX, toY);
    }

    isSomethingBetween(x1, y1, x2, y2) {
        this._line = this._line || new Phaser.Line();
        let result = [];
        this._line.setTo((x1/PASS_CELL_SIZE)|0, (y1/PASS_CELL_SIZE)|0, (x2/PASS_CELL_SIZE)|0, (y2/PASS_CELL_SIZE)|0);
        this._line.coordinatesOnLine(1, result);
        let passable = true;
        for (let [gx,gy] of result) {
            let cell = this.getCell(gx, gy);
            if (!cell || cell.wall || cell.passable === false) {
                passable = false;
                break;
            }
        }
        return !passable;
    }

    getPassablePointInRadius(x, y, radiusFrom, radiusTo, forHuman = false) {
        let cell = null;
        let nx, ny, dist, limit = 20;

        do {
            nx = game.rnd.integerInRange(x - radiusTo, x + radiusTo);
            ny = game.rnd.integerInRange(y - radiusTo, y + radiusTo);
            dist = Math.hypot(x-nx,y-ny);
            cell = this.getCellForPoint(nx, ny);
            limit--;
            if (limit < 0) break;
        } while (!cell || cell.wall || (forHuman ? cell.passable === false : cell.passableForMonster === false) || dist < radiusFrom || dist > radiusTo || limit < 0);
        return cell ? {x: cell.x * PASS_CELL_SIZE, y: cell.y * PASS_CELL_SIZE} : undefined;
    }

    findDoorForward(x1, y1, x2, y2) {
        this._line = this._line || new Phaser.Line();
        let result = [];
        this._line.setTo((x1/PASS_CELL_SIZE)|0, (y1/PASS_CELL_SIZE)|0, (x2/PASS_CELL_SIZE)|0, (y2/PASS_CELL_SIZE)|0);
        this._line.coordinatesOnLine(1, result);
        for (let [gx,gy] of result) {
            let cell = this.getCell(gx, gy);
            if (cell.door) return cell.doorSprite;
        }
        return null;
    }

    createMonsterFinder() {
        if (this._monsterFinder) return this._monsterFinder;
        let graph = createGraph();

        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                let cell = this.getCell(x, y);
                if (!cell.wall) {
                    graph.addNode(cell.id, cell);
                    //add links only to prev nodes
                    [
                        {x:-1,y:-1},{x:0,y:-1},{x:1,y:-1},{x:-1,y:0}
                    ].map(({x,y}) => this.getCell(cell.x+x, cell.y+y)).filter(c => c && !c.wall).forEach(nei => {
                        graph.addLink(cell.id, nei.id);
                    })
                }
            }
        }

        graph.forEachLinkedNode = forEachLinkedNodePatchedFactory(node => node.data.passableForMonster !== false);


        let pathFinder = path.aStar(graph, {
            //heuristic: path.aStar.l1,
            distance(node1, node2) {
                return Phaser.Point.distance(node1.data, node2.data);
            }
        });

        return this._monsterFinder = (fromX, fromY, toX, toY) => {
            let fromCell = this.getCellForPoint(fromX, fromY);
            let toCell = this.getCellForPoint(toX, toY);
            if (!fromCell || !toCell || toCell.wall || toCell.passableForMonster === false) {
                console.warn("no way", fromX, fromY, "->", toX, toY);
                return [];
            }
            if (fromCell.wall || fromCell.passableForMonster === false) {
                //ok, look for neighbours
                for (let {x, y} of [{x:-1,y:0},{x:1,y:0},{x:0,y:1},{x:0,y:-1}]) {
                    let nei = this.getCell(fromCell.x + x, fromCell.y + y);
                    if (nei && !nei.wall) {
                        fromCell = nei;
                        break;
                    }
                }
            }


            if (fromCell.id === toCell.id) {
                //same cell. just move
                return [{x: toX, y: toY}];
            }

            let cells = pathFinder.find(fromCell.id, toCell.id).reverse().map(n => n.data);

            cells = this.optimizeUnitsPath(cells);

            if (cells.some(c => !c)) {
                console.warn("no cell in result?", cells, fromCell.id, toCell.id);
            }

            cells = cells.map(cell => ({
                x: cell.x * PASS_CELL_SIZE + PASS_CELL_SIZE/2,
                y: cell.y * PASS_CELL_SIZE + PASS_CELL_SIZE/2,
            }));
            if (cells.length) {
                cells.pop();
                cells.push({x: toX, y: toY});
            }
            return cells;
        }
    }

    //todo: сделать клеткам рядом с проходом веса побольше:
    /*
        ####
       !####

       !####
        ####

    вот где ! отмечено. это чтобы юниты чуть раньше сворачивали и углы огибали

     */
    createUnitFinder() {
        if (this._unitFinder) return this._unitFinder;
        //units cannot move through doors.
        //returns [{x,y}, {x,y}, ...] for straight moving. unit itself shall add some noise for own moving
        let graph = createGraph();

        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                let cell = this.getCell(x, y);
                if (!cell.wall) {
                    graph.addNode(cell.id, cell);
                    //add links only to prev nodes
                    [
                        {x:-1,y:-1},{x:0,y:-1},{x:1,y:-1},{x:-1,y:0}
                    ].map(({x,y}) => this.getCell(cell.x+x, cell.y+y)).filter(c => c && !c.wall).forEach(nei => {
                        graph.addLink(cell.id, nei.id);
                    })
                }
            }
        }

        graph.forEachLinkedNode = forEachLinkedNodePatchedFactory(node => node.data.passable !== false);

        let pathFinder = path.aStar(graph, {
            //heuristic: path.aStar.l1,
            distance(node1, node2) {
                return Phaser.Point.distance(node1.data, node2.data);
            }
        });

        return this._unitFinder = (fromX, fromY, toX, toY) => {
            let fromCell = this.getCellForPoint(fromX, fromY);
            let toCell = this.getCellForPoint(toX, toY);
            if (!fromCell || !toCell || toCell.wall || toCell.passable === false) {
                console.warn("no way", fromX, fromY, "->", toX, toY);
                return [];
            }
            if (fromCell.wall) {
                //ok, look for neighbours
                for (let {x, y} of [{x:-1,y:0},{x:1,y:0},{x:0,y:1},{x:0,y:-1}]) {
                    let nei = this.getCell(fromCell.x + x, fromCell.y + y);
                    if (nei && !nei.wall) {
                        fromCell = nei;
                        break;
                    }
                }
            }

            if (fromCell.id === toCell.id) {
                //same cell. just move
                return [{x: toX, y: toY}];
            }

            let cells = pathFinder.find(fromCell.id, toCell.id).reverse().map(n => n.data);

            cells = this.optimizeUnitsPath(cells);

            if (cells.some(c => !c)) {
                console.warn("no cell in result?", cells, fromCell.id, toCell.id);
            }

            cells = cells.map(cell => ({
                x: cell.x * PASS_CELL_SIZE + PASS_CELL_SIZE/2,
                y: cell.y * PASS_CELL_SIZE + PASS_CELL_SIZE/2,
            }));
            if (cells.length) {
                cells.pop();
                cells.push({x: toX, y: toY});
            }
            return cells;
        }
    }

    optimizeUnitsPath(cells) {
        if (cells.length === 0) return cells;
        //return cells;
        let otrezki = [];
        //first - define big otrezki

        let p1 = cells[0];
        let otrezok = {
            from: p1,
            to: null
        };
        otrezki.push(otrezok);
        let prevDirection = null, prevPoint = p1;
        for (let i = 1; i < cells.length; i++) {
            let p2 = cells[i];
            let thisDirection = {x: p2.x - prevPoint.x, y: p2.y - prevPoint.y};
            if (!prevDirection) {
                prevDirection = thisDirection;
                otrezok.to = p2;
            } else if (prevDirection.x === thisDirection.x && prevDirection.y === thisDirection.y) {
                otrezok.to = p2;
            } else {
                otrezok = {from: prevPoint, to: p2};
                otrezki.push(otrezok);
                prevDirection = thisDirection;
            }
            prevPoint = p2;
        }


        //next - try to straight otrezki. later

        let line = new Phaser.Line();
        let result = [];
        for (let oi = otrezki.length-1; oi > 0; oi--) {
            let ot2 = otrezki[oi];
            let ot1 = otrezki[oi-1];
            let possibleOtrezok = {
                from: ot1.from,
                to: ot2.to
            };
            line.setTo(possibleOtrezok.from.x*PASS_CELL_SIZE, possibleOtrezok.from.y*PASS_CELL_SIZE, possibleOtrezok.to.x*PASS_CELL_SIZE, possibleOtrezok.to.y*PASS_CELL_SIZE);
            line.coordinatesOnLine(PASS_CELL_SIZE/8, result);
            let passable = true;
            for (let [x,y] of result) {
                let cell = this.getCellForPoint(x, y);
                if (!cell || cell.wall || cell.passable === false) {
                    passable = false;
                    break;
                }
            }
            if (passable) {
                otrezki.splice(oi-1, 2, possibleOtrezok);
            }
        }


        return otrezki.map(o => o.to);


    }



}



function forEachNonOrientedLink(links, nodeId, callback, getNode, isNodePassable) {
    var quitFast;
    for (var i = 0; i < links.length; ++i) {
        var link = links[i];
        var linkedNodeId = link.fromId === nodeId ? link.toId : link.fromId;

        var node = getNode(linkedNodeId);
        if (!isNodePassable(node)) continue;
        quitFast = callback(node, link);
        if (quitFast) {
            return true; // Client does not need more iterations. Break now.
        }
    }
    return undefined;
}

function forEachLinkedNodePatchedFactory(isNodePassable) { return function forEachLinkedNodePatched(nodeId, callback, oriented) {
    var node = this.getNode(nodeId);

    if (node && node.links && typeof callback === 'function') {
        if (!oriented) {
            return forEachNonOrientedLink(node.links, nodeId, callback, this.getNode.bind(this), isNodePassable);
        } else {
            throw new Error("not implemented")
        }
    }
}}
