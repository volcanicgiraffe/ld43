import { CAMPAIGN } from "../../consts";

export default class GameEnder {
    constructor() {
        if (game.level.exitZone === undefined) console.warn("No Exit Zone Found! Pls add one.");
    }

    update() {
        if (this._done) return;
        if (game.level.exitZone === undefined) return;

        if (game.level.unitsGrp.children.length === 0) {
            if (typeof ga === "function") ga('send', 'event', 'game_end', game.level.levelName, 'lose');
            this.levelFail();
        }
        if (!game.level.pauseText.visible && game.anyInput.pressedEvacuate() && (game.level.exitZone.peopleEvacuating.length > 0)) {
            if (typeof ga === "function") ga('send', 'event', 'game_end', game.level.levelName, 'win');
            this.levelWin();
        }
    }

    levelFail() {
        this.finitaLaComedia('lose');
    }

    levelWin() {
        if (game.level && game.level.exitZone && game.level.exitZone.peopleEvacuating.length > 0) {
            game.storage.saveProfiles(game.level.exitZone.peopleEvacuating);
        }
        this.finitaLaComedia('win');
    }

    finitaLaComedia(status) {
        if (this._done || game.level.completed) return;
        this._done = true;
        game.level.completed = true;

        if (status === 'win') {
            game.camera.flash(0xDDDDDD, 700);
            game.sounder.play('end_2');

            this._reportColors();

            game.storage.addToDeadList(game.level.deadPeople);

            try {
                let notRescued = game.level.decalGrp.children.filter((ch) => ch.profile != null).map((u) => u.profile.name);
                game.storage.addToDeadList(notRescued);
            } catch (e) {
            }
            try {
                let notEvacuated = game.level.unitsGrp.children.filter(u => !u.state.evacuated).map((u) => u.state.profile.name);
                game.storage.addToDeadList(notEvacuated);
            } catch (e) {
            }
            game.storage.saveGameTime(game.storage.loadGameTime() + Math.ceil(game.level.levelTimer / 1000));
            game.storage.saveLevelIndex(game.storage.loadLevelIndex() + 1);
        } else {
            game.camera.flash(0x880000, 700);
            game.sounder.play('end_2');
        }
        game.level.unitsGrp.destroy();

        game.time.events.add(720, () => {
            game.camera.fade(0x000000, 1000);

            game.time.events.add(1000, () => {
                game.world.removeAll(true, false);

                if (game.storage.loadLevelIndex() >= CAMPAIGN.length) {
                    game.state.start("win");
                } else {
                    game.state.start("level_state");
                }
            })
        })
    }

    _reportColors() {
        if (typeof ga !== "function") return;

        try {
            let counts = {};
            game.level.unitsGrp.children.map((k) => k.state.profile.type).forEach((x) => {
                counts[x] = (counts[x] || 0) + 1;
            });

            for (let color in counts) {
                if (!counts.hasOwnProperty(color)) continue;
                ga('send', 'event', 'level_win', game.level.levelName, color, counts[color]);
            }
        } catch (e) {
            // I don't care
        }
    }
}
