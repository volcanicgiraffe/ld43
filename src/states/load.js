import PadManager from "../helpers/pad_manager";
import AnyInput from "../helpers/any_input";
import Sounder from "../helpers/sounder";
import StorageManager from "../helpers/storage_manager";

export default {
    loadingLabel() {
        let loading = game.add.sprite(game.world.centerX, game.world.centerY + 100, "loading");
        loading.anchor.setTo(0.5, 0.5);

        let bar = game.add.sprite(game.world.centerX, game.world.centerY - 45, "loading_giraffe");
        bar.anchor.setTo(0.5, 1);

        let barBg = game.add.sprite(game.world.centerX, game.world.centerY, "loading_island");
        barBg.anchor.setTo(0.5, 0.5);

        game.load.setPreloadSprite(bar, 1);
    },

    preload() {
        this.loadingLabel();
        game.load.spritesheet("test", "assets/danger.png", 32, 32);
        game.load.image("tiles", "assets/levels/tiles_32.png");
        game.load.image("title", "assets/title.png");
        game.load.image("help", "assets/help.png");
        game.load.image("background", "assets/background.png");
        game.load.atlas("enemy", "assets/enemy.png", "assets/enemy.json");
        game.load.atlas("decals", "assets/decals.png", "assets/decals.json");
        game.load.atlas("objects", "assets/objects.png", "assets/objects.json");
        game.load.atlas("human", "assets/human.png", "assets/human.json");
        game.load.atlas("UI", "assets/UI.png", "assets/UI.json");
        game.load.bitmapFont('font24', 'assets/font_24-export.png', 'assets/font_24-export.xml');
        game.load.bitmapFont('font36', 'assets/font_36-export.png', 'assets/font_36-export.xml');
        this.patch();

        this._loadAudio();
        this._loadLevels();
    },

    create() {
        game.canvas.oncontextmenu = () => false;

        game.storage = new StorageManager();
        game.sounder = new Sounder();

        game.input.gamepad.start();
        game.pad = new PadManager();
        game.anyInput = new AnyInput();

        game.state.start("menu");

        this._theme = game.add.sound('ambient');
        this._theme.play(undefined, undefined, 0, true);
        this._theme.volume = 0.5;
    },


    patch() {
        let coloredBixelsMap = new Map();

        function getBitmap(color) {
            let bm = coloredBixelsMap.get(color);
            if (!bm) {
                bm = game.add.bitmapData(1, 1);
                bm.ctx.fillStyle = color;
                bm.ctx.fillRect(0, 0, 1, 1);
                bm.dirty = true;
                coloredBixelsMap.set(color, bm);
            }
            return bm;
        }

        game.getColoredPixel = function (color) {
            return getBitmap(color);
        };
        Phaser.GameObjectFactory.prototype.spriteFilled = function (x, y, width, height, color) {
            let sp = game.add.sprite(x, y, game.getColoredPixel(color));
            sp.width = width;
            sp.height = height;

            sp.changeColor = function (color) {
                this.loadTexture(game.getColoredPixel(color), 0);
            };
            return sp;
        };
    },

    _loadAudio() {
        game.load.audio('ambient', 'assets/audio/ambient.mp3');

        game.load.audio('bang_1', 'assets/audio/bang_1.mp3');
        game.load.audio('bang_2', 'assets/audio/bang_2.mp3');
        game.load.audio('bang_3', 'assets/audio/bang_3.mp3');

        game.load.audio('beat_1', 'assets/audio/beat_1.mp3');
        game.load.audio('beat_2', 'assets/audio/beat_2.mp3');
        game.load.audio('beat_3', 'assets/audio/beat_3.mp3');

        game.load.audio('boop_1', 'assets/audio/boop_1.mp3');

        game.load.audio('click_1', 'assets/audio/click_1.mp3');
        game.load.audio('click_2', 'assets/audio/click_2.mp3');
        game.load.audio('click_3', 'assets/audio/click_3.mp3');
        game.load.audio('click2', 'assets/audio/click2.wav');

        game.load.audio('death_1', 'assets/audio/death_1.mp3');
        game.load.audio('death_2', 'assets/audio/death_2.mp3');
        game.load.audio('death_3', 'assets/audio/death_3.mp3');

        game.load.audio('door_1', 'assets/audio/door_1.mp3');
        game.load.audio('door_2', 'assets/audio/door_2.mp3');
        game.load.audio('door_3', 'assets/audio/door_3.mp3');

        game.load.audio('door_close_1', 'assets/audio/door_close_1.mp3');
        game.load.audio('door_close_2', 'assets/audio/door_close_2.mp3');
        game.load.audio('door_close_3', 'assets/audio/door_close_3.mp3');

        game.load.audio('end_1', 'assets/audio/end_1.mp3');
        game.load.audio('end_2', 'assets/audio/end_2.mp3');

        game.load.audio('menu', 'assets/audio/menu.mp3');

        game.load.audio('roar_1', 'assets/audio/roar_1.mp3');
        game.load.audio('roar_2', 'assets/audio/roar_2.mp3');
        game.load.audio('roar_3', 'assets/audio/roar_3.mp3');
        game.load.audio('roar_4', 'assets/audio/roar_4.mp3');
        game.load.audio('roar_5', 'assets/audio/roar_5.mp3');

        game.load.audio('spider_steps_hi', 'assets/audio/spider_steps_hi.mp3');
        game.load.audio('spider_steps_low', 'assets/audio/spider_steps_low.mp3');

        game.load.audio('step_a_1', 'assets/audio/step_a_1.mp3');
        game.load.audio('step_a_2', 'assets/audio/step_a_2.mp3');
        game.load.audio('step_a_3', 'assets/audio/step_a_3.mp3');

        game.load.audio('step_b_1', 'assets/audio/step_b_1.mp3');
        game.load.audio('step_b_2', 'assets/audio/step_b_2.mp3');
        game.load.audio('step_b_3', 'assets/audio/step_b_3.mp3');

        game.load.audio('step_m_1', 'assets/audio/step_m_1.mp3');
        game.load.audio('step_m_2', 'assets/audio/step_m_2.mp3');
        game.load.audio('step_m_3', 'assets/audio/step_m_3.mp3');

        game.load.audio('footsteps', 'assets/audio/footsteps.mp3');
        game.load.audio('sneeze', 'assets/audio/sneeze.wav');
        game.load.audio('gasp', 'assets/audio/gasp.mp3');
        game.load.audio('fall', 'assets/audio/fall.wav');
        game.load.audio('loudnoise1', 'assets/audio/noise_m.mp3');
        game.load.audio('loudnoise2', 'assets/audio/noise_m2.mp3');
        game.load.audio('panic_1', 'assets/audio/panic_yell.mp3');
        game.load.audio('aracnho-1', 'assets/audio/aracnho-1.mp3');
        game.load.audio('aracnho-2', 'assets/audio/aracnho-2.mp3');

    },

    _loadLevels() {
        game.load.tilemap('LevelZero', 'assets/levels/LevelZero.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelNoises', 'assets/levels/LevelNoises.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelScared', 'assets/levels/LevelScared.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelDoggie', 'assets/levels/LevelDoggie.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelSacrifice', 'assets/levels/LevelSacrifice.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelDilemma', 'assets/levels/LevelDilemma.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelSimple', 'assets/levels/LevelSimple.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelTest', 'assets/levels/LevelTest.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelLetmein', 'assets/levels/LevelLetmein.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelRun', 'assets/levels/LevelRun.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('LevelCoolants', 'assets/levels/LevelCoolants.json', null, Phaser.Tilemap.TILED_JSON);


        game.load.tilemap('AlexeyPolygon', 'assets/levels/AlexeyPolygon.json', null, Phaser.Tilemap.TILED_JSON);
    }
};
