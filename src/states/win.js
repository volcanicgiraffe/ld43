import { toMmSs } from "../helpers/math";

module.exports = {
    init() {
        game.world.alpha = 1;
        game.world.setBounds(0, 0, 1280, 720);
        game.stage.backgroundColor = '#27282D';

    },

    create() {
        game.stage.backgroundColor = '#0F0000';
        game.world.setBounds(0, 0, 1280, 720);
        game.add.tween(game.world).to({ alpha: 1 }, 100, null, true);

        this.bg = game.add.sprite(0, 0, 'title');
        this.bg.width = game.world.width;
        this.bg.x -= this.bg.width * 0.125;
        this.bg.y -= this.bg.height * 0.125;
        this.bg.tint = 0x444444;

        this.bg.scale.set(1.25);

        let text1 = game.add.bitmapText(100, 100, "font36", "YOU HAVE SURVIVED", 36);
        text1.scale.set(2.0);

        let text2 = game.add.bitmapText(100, 210, "font36", "COMPLETION TIME: " + toMmSs(game.storage.loadGameTime() * 1000), 36);
        let text3 = game.add.bitmapText(100, 250, "font36", "PEOPLE SAVED: " + game.storage.loadProfiles().length, 36);

        let names = game.storage.loadDeadList();

        let offsetY = Math.max(10,(game.world.height - names.length * 40) / 2);

        let text4 = game.add.bitmapText(game.world.width - 100, offsetY, "font36", "Sacrifices will never be forgotten", 36);
        text4.anchor.set(1, 0);

        offsetY += 50;

        for (let n of names) {
            let nametext = game.add.bitmapText(game.world.width - 100, offsetY, "font36", n, 36);
            nametext.anchor.set(1, 0);
            offsetY += 40;
        }

        game.input.keyboard.enabled = true;
        this.bg.inputEnabled = true;
        this.bg.input.useHandCursor = true;
        let next = () => {
            game.input.keyboard.onDownCallback = undefined;
            game.add.tween(game.world)
                .to({ alpha: 0 }, 100, null, true)
                .onComplete
                .add(() => game.state.start("menu"));
        };
        this.bg.events.onInputUp.addOnce(next);
        //game.input.keyboard.onDownCallback = next;

        if (typeof ga === "function") ga('send', 'event', 'state', 'win');
    }
};
