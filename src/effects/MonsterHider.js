export default class MonsterHider {

    constructor() {
        this.renderTexture = game.add.renderTexture(game.camera.width, game.camera.height); //ouch
        this.sprite = game.add.sprite(100000, 0, this.renderTexture);
        //this.sprite.fixedToCamera = true;
        this.group = game.add.group(undefined, "monsters pseudo group");
        game.world.sendToBack(this.group);
        //this.group.alpha = 0.01;
        //this.group.x = 10000;

        //this.sprite.tint = 0x00ff00;
    }

    get children() {
        return this.group.children;
    }

    add(monster) {
        this.group.add(monster);
        //monster.alpha = 0;
    }

    update() {
        //this.group.alpha = 1;
        this.renderTexture.clear();
        for (let m of this.group.children) {
            this.renderTexture.renderXY(m, m.x - game.camera.x, m.y - game.camera.y);
        }
        //this.group.alpha = 0;
    }
}