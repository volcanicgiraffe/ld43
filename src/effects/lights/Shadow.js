

/*

1. just sprites
2. draw on small texture


 */

import {STATE_NORMAL, STATE_PANIC} from "../../objects/units/unit";
import {PIX} from "../../consts"
import Unit from "../../objects/units/unit";
import {reach} from "../../helpers/math";


class ManShadowModel {
    constructor(unitSprite) {
        this.sprite = unitSprite;
        this.legs = [
            {x: -10, y: 0, z: 0, radius: 4},
            {x: 10, y: 0, z: 0, radius: 4}
        ];
        this._p = new Phaser.Point();

        this.scaleX = 1;
        this.scaleY = 1;
    }

    stepLegs(left, right) {
        this.legs[0].z = reach(this.legs[0].z, left, 0.1);
        this.legs[1].z = reach(this.legs[1].z, right, 0.1);
    }

    update() {
        this._p.set(0, -4);
        this._p.rotate(0, 0, this.sprite.rotation);
        this.legs[0].x = this.sprite.centerX + this._p.x;
        this.legs[0].y = this.sprite.centerY + this._p.y;

        this._p.set(0, 4);
        this._p.rotate(0, 0, this.sprite.rotation);
        this.legs[1].x = this.sprite.centerX + this._p.x;
        this.legs[1].y = this.sprite.centerY + this._p.y;

        switch (this.sprite.frameName.split('_')[1]) {
            case 'w1.png':
                this.stepLegs(1, 0); break;
            case 'w2.png':
                this.stepLegs(2, 0); break;
            case 'w3.png':
                this.stepLegs(1, 0); break;
            case 'w4.png':
                this.stepLegs(0, 0); break;
            case 'w5.png':
                this.stepLegs(0, 1); break;
            case 'w6.png':
                this.stepLegs(0, 2); break;
            case 'w7.png':
                this.stepLegs(0, 1); break;
            case 'w8.png':
            case 'idle.png':
            default:
                this.stepLegs(0, 0); break;
            //? dying?
        }
    }

    isAlive() {
        return this.sprite.alive && Unit.canDropShadow(this.sprite.state);
    }
}

class MonsterShadowModel {
    constructor(monsterSprite) {
        this.sprite = monsterSprite;
        this.legs = [
            {x: 0, y: 0, z: 0},
            {x: 0, y: 0, z: 0},
            {x: 0, y: 0, z: 0},
        ];
        this.scaleX = 1.2;
        this.scaleY = 2;

    }

    update() {
        let mlegs = this.sprite.monsterParts.legs;
        for (let i = 0; i < 3; i++) {
            this.legs[i].x = mlegs[i].model.x2;
            this.legs[i].y = mlegs[i].model.y2;
            this.legs[i].z = mlegs[i].model.z*2;
        }
    }

    isAlive() {
        return this.sprite.alive;
    }
}

export const SHADOW_ZM = 2;
export class ShadowDrawer {

    constructor(group, width, height) {
        this.realGroup = group;

        this.group = game.add.group();
        game.world.remove(this.group);

        this.staticLights = [];
        this.dynamicLights = [];

        game.level.state.global.onChange(g => g.activeUnitsCountChanged, this.updateDynamicLights.bind(this));

        this.shadowModels = [];

        this._v = new Phaser.Point();

        this.texture = game.add.renderTexture(width / SHADOW_ZM, height / SHADOW_ZM);
        this.group.scale.set(1/SHADOW_ZM);

        this.sprite = game.add.sprite(-1000, 0, this.texture);
        //this.sprite.scale.set(SHADOW_ZM);

    }

    debug() {
        //this.sprite.x = 0;
        //this.sprite.scale.set(SHADOW_ZM);
        //this.sprite.alpha = 0.5;
        //game.world.bringToTop(this.sprite);
    }

    onAddUnit(unit) {
        this.shadowModels.push(new ManShadowModel(unit));
        this.updateDynamicLights();
    }

    init() {
        this.updateDynamicLights();

        for (let unit of game.level.unitsGrp.children) {
            this.shadowModels.push(new ManShadowModel(unit));
        }
        for (let monster of game.level.monsterGrp.children) {
            this.shadowModels.push(new MonsterShadowModel(monster));
        }
    }

    registerStaticLight(light) {
        if (light.dropShadow) {
            this.staticLights.push(light);
        }
    }

    updateDynamicLights() {
        this.dynamicLights = game.level.unitsGrp.children.map(unit => unit.state).filter(u => u.mainState === STATE_NORMAL || u.mainState === STATE_PANIC).map(u => ({
            x: u.light.x,
            y: u.light.y,
            lightStrength: 0.08,
            lightRadius: 8*PIX
        }));
    }


    update() {
        this._spriteIterator = 0;
        for (let shm of this.shadowModels) {
            if (!shm.isAlive()) continue;
            shm.update();

            for (let light of this.staticLights) {
                if (Phaser.Point.distance(light, shm.sprite) < light.lightRadius) {
                    this.dropShadow(shm, light);
                }
            }
            for (let light of this.dynamicLights) {
                if (Phaser.Point.distance(light, shm.sprite) < light.lightRadius) {
                    this.dropShadow(shm, light);
                }
            }
        }

        for (let i = this._spriteIterator; i < this.group.children.length; i++) {
            this.group.children[i].kill();
        }

        this.texture.renderXY(this.group, 0, 0, true);
    }

    dropShadow(model, light) {
        const LHEIGHT = 12;
        for (let leg of model.legs) {
            let shadow = this._pickSprite();
            this._v.set(light.x - leg.x, light.y - leg.y);
            let dist = this._v.getMagnitude();
            this._v.normalize();

            shadow.rotation = this._v.angleXY(0, 0) + Math.PI/2;
            shadow.alpha = (1 - dist / light.lightRadius) * light.lightStrength;

            this._v.setMagnitude(-leg.z * LHEIGHT);
            shadow.x = leg.x + this._v.x;
            shadow.y = leg.y + this._v.y;
            shadow.scale.x = model.scaleX;
            shadow.scale.y = model.scaleY;
        }
    }

    _pickSprite() {
        let sprite = this.group.children[this._spriteIterator];
        if (!sprite) {
            //sprite = game.make.sprite(0, 0, "enemy", "leg_shadow.png");
            sprite = game.make.sprite(0, 0, (ShadowDrawer._legBitmap || (ShadowDrawer._legBitmap = createLegBitmap())));
            this.group.add(sprite);
            sprite.anchor.set(0.5, 1);
        }
        sprite.revive();

        this._spriteIterator++;

        return sprite;
    }


}


function createLegBitmap() {
    let bitmap = game.add.bitmapData(32, 160);
    let g = bitmap.ctx.createLinearGradient(16, 0, 16, 160);
    g.addColorStop(0, "rgba(0,0,255,0)");
    g.addColorStop(1, "rgba(0,0,255,1)");
    bitmap.ctx.fillStyle = g;
    bitmap.ctx.moveTo(8, 159);
    bitmap.ctx.lineTo(0, 0);
    bitmap.ctx.lineTo(31, 0);
    bitmap.ctx.lineTo(24, 159);
    bitmap.ctx.fill();

    return bitmap;
}