

/*
    Light could be drawn in world. Could be not.

    Light texture will contain only
        a) glowing parts
        b) "anti-shadow" parts (lighted)


    любой объект может попасть на lighttexture

    light либо не рисуется вообще (если нет фрейма), либо запекается в текстуру тоже
        хотя вот это не обязательно, хоть и омжно

 */


export default class Light {

    constructor(x, y, width, height, frame, properties = {}) {
        this.lightRadius = properties['light-radius'] || 0;
        this.lightAngle = properties['light-angle'] !== undefined ? Phaser.Math.degToRad(properties['light-angle']) : undefined;
        this.lightSpread = properties['light-spread'] !== undefined  ? Phaser.Math.degToRad(properties['light-spread']) : undefined;
        this.lightStrength = properties['light-strength'] || 1;
        this.lightColor = properties['light-color'];
        this.dropShadow = properties['drop-shadows'] || false;

        if (frame) {
            this.sprite = game.make.sprite(x, y, "objects", frame);
            this.sprite.anchor.set(0.5, 0.5);

            if (properties.rotation === undefined) {
                properties.rotation = this.lightAngle;
            } else {
                properties.rotation = Phaser.Math.degToRad(properties.rotation);
            }
            if (properties.rotation !== undefined) {
                this.sprite.rotation = properties.rotation;
            }

            this.x = this.sprite.centerX;
            this.y = this.sprite.centerY;
        } else {
            this.x = x;
            this.y = y;
        }

        game.level.lightTexture.registerLight(this);

    }

}