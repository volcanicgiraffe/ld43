import {ShadowDrawer} from "./Shadow";

export default class LightTexture {
    //knows what to draw over shadow

    constructor(grp, shadowGrp, width, height) {
        this.group = grp;
        this.bitmap = game.add.bitmapData(width, height);
        this.sprite = game.add.sprite(-10000, -10000, this.bitmap);
        this.sprite.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;

        this.shadowDrawer = new ShadowDrawer(shadowGrp, width, height);
        this.LightMasks = new Map();
        this.tempSprite = game.add.sprite(-10000, -1000, "objects");
    }

    update() {
        this.shadowDrawer.update();
    }

    registerLight(light) {
        //return;
        this.shadowDrawer.registerStaticLight(light);
        if (light.sprite) {
            this.group.add(light.sprite);
            this.registerObjectWithLights(light.sprite);
        }
        if (light.lightRadius) {
            let ctx = this.bitmap.ctx;
            let gradient = ctx.createRadialGradient(light.x, light.y, 0, light.x, light.y, light.lightRadius);
            let g = (light.lightStrength*255)|0;
            gradient.addColorStop(0, `rgb(0,0,${g})`);
            gradient.addColorStop(0.5, `rgba(0,0,${g},0.1)`);
            gradient.addColorStop(1, `rgba(0,0,${g},0)`);
            ctx.beginPath();
            ctx.globalCompositeOperation = "lighter";
            ctx.fillStyle = gradient;
            ctx.moveTo(light.x, light.y);
            if (light.lightAngle !== undefined) {
                let angle1 = light.lightAngle - light.lightSpread/2;
                let angle2 = light.lightAngle + light.lightSpread/2;
                ctx.lineTo(
                    light.x + Math.cos(angle1)*light.lightRadius,
                    light.y + Math.sin(angle1)*light.lightRadius
                );
                ctx.arc(light.x, light.y, light.lightRadius, angle1, angle2);
                ctx.lineTo(light.x, light.y);
            } else {
                ctx.arc(light.x, light.y, light.lightRadius, 0, Math.PI*2);
            }

            ctx.fill();
        }
    }

    registerObjectWithLights(object, frameForMask = object.frameName, isMaskItself = false) {
        let mask = getLightMask(this, object, frameForMask, isMaskItself);
        if (mask) {
            mask.rotation = object.rotation;
            mask.x = object.x;
            mask.y = object.y;
            mask.anchor.x = object.anchor.x;
            mask.anchor.y = object.anchor.y;
            //this.bitmap.draw(mask, 100, 100, 100, 100);
            //game.add.sprite(0, 0, mask.key, mask.frameName);
            this.bitmap.draw(mask, undefined, undefined, undefined, undefined, "lighter");
            //this.bitmap.copy(mask, object.left, object.top, undefined, undefined, undefined, undefined, undefined, undefined, object.rotation, undefined, undefined, undefined, undefined, undefined, "lighter", undefined);
        }
        let glowSprite = getGlowSprite(object);
        if (glowSprite) {
            if (object.addGlowSprite) {
                object.addGlowSprite(glowSprite);
            } else {
                object.addChild(glowSprite);
                glowSprite.anchor.set(object.anchor.x, object.anchor.y);
                glowSprite.update = () => {
                    glowSprite.frameName = getGlowFrameName(object);
                }
            }
        }
    }

    onAddUnit(unit) {
        this.shadowDrawer.onAddUnit(unit);
    }

    debug() {
        game.world.bringToTop(this.sprite);
        this.sprite.x = 0;
        this.sprite.y = 0;
        this.sprite.alpha = 0.5;
        this.shadowDrawer.debug();
    }

    init() {
        this.shadowDrawer.init();
    }
}

const NEED_REPAINT = true;

function getGlowSprite(sprite) {
    let frames = game.cache.getImage(sprite.key, true).frameData._frameNames;
    let possibleMaskFrame = "LMS1_" + sprite.frameName;
    if (frames[possibleMaskFrame] !== undefined) {
        return game.make.sprite(0, 0, sprite.key, possibleMaskFrame);
    }
    return null;
}

function getGlowFrameName(sprite) {
    return "LMS1_" + sprite.frameName;
}

function createLightMask({tempSprite}, sprite, frameName = sprite.frameName, isMaskItself) {
    let bitmap = game.add.bitmapData(sprite.width, sprite.height);
    bitmap.sprite = game.make.sprite(0, 0, bitmap);
    let oldRotation = sprite.rotation;
    let oldAnchorX = sprite.anchor.x;
    let oldAnchorY = sprite.anchor.y;

    //lookup for texture
    let frames = game.cache.getImage(sprite.key, true).frameData._frameNames;
    let possibleMaskFrame = isMaskItself ? frameName : "LMS1_" + frameName;
    let anyPixel = false;
    if (frames[possibleMaskFrame] !== undefined) {

        tempSprite.frameName = possibleMaskFrame;
        //console.log(frameName, tempSprite.width, tempSprite.height, tempSprite._frame);
        //sprite.rotation = 0;
        //sprite.anchor.set(0,0);
        bitmap.draw(tempSprite, 0, 0);
        //sprite.anchor.set(oldAnchorX, oldAnchorY);
        //sprite.frameName = frameName;
        //sprite.rotation = oldRotation;
        if (!NEED_REPAINT) return bitmap.sprite;
        anyPixel = true;
    }
    else {
        tempSprite.frameName = sprite.frameName;
        //sprite.rotation = 0;
        //sprite.anchor.set(0,0);
        bitmap.draw(tempSprite, -tempSprite.left, -tempSprite.top);
        //sprite.rotation = oldRotation;
        //sprite.anchor.set(oldAnchorX, oldAnchorY);
    }
    bitmap.update();

    let i = bitmap.imageData.data.length - 4;
    let data = bitmap.imageData.data;
    let r,g,b,a;
    for (;i >= 0; i-= 4) {
        r = data[i];
        g = data[i+1];
        b = data[i+2];
        a = data[i+3];
        if ((anyPixel && a > 0) || (r > g && r > b && r > 10 && a > 0) || (g > r && g > b && g > 128 && a > 0)) {
            data[i] = 255;
            data[i+1] = 0;
            data[i+2] = 0;
            data[i+3] = a;
        } else {
            data[i] = data[i+1] = data[i+2] = data[i+3] = 0;
        }
    }
    bitmap.context.putImageData(bitmap.imageData, 0, 0);
    return bitmap.sprite;
}

function getLightMask({LightMasks, tempSprite}, sprite, frameName = sprite.frameName, isMaskItself) {
    return LightMasks.get(frameName) || ((LightMasks[frameName] = createLightMask({tempSprite}, sprite, frameName, isMaskItself)));
}
