import {PIX} from "../consts";
import {SHADOW_ZM} from "./lights/Shadow";

const LIGHT_RADIUS = PIX*4;
export const BIG_LIGHT_RADIUS = PIX*8;
const LIGHT_TREMBLE = 3;

const DYING_RADIUS = PIX*3.5;
/*

вариант другой:
    - создаем render texture для паука и света
    - вот к ней применяем шейдер. можно даже последовательность
        блум
        размытие
        туман войны + поук
    - вешаем сверху, над игровым полем

 */

export default class Atmosphere extends Phaser.Filter {

    constructor() {
        super(game);

        let array = [];
        array.length = 80;
        array.fill(0, 0, 80);
        this.uniforms.lights = {value: array, type: "4fv"};
        this.uniforms.lightsCount = {value: 0, type: "1i"};

        let array2 = [];
        array2.length = 80;
        array2.fill(0, 0, 80);
        this.uniforms.noises = {value: array2, type: "4fv"};
        this.uniforms.noisesCount = {value: 0, type: "1i"};
        this.uniforms.iChannel0.value = game.level.monsterHider.sprite.texture;
        this.uniforms.iChannel0.textureData = undefined;
        this.uniforms.iChannel1.value = game.level.uiMask.sprite.texture;
        this.uniforms.iChannel1.textureData = undefined;
        this.uniforms.iChannel2.value = game.level.lightTexture.sprite.texture;
        this.uniforms.iChannel2.textureData = {
            magFilter: game.renderer.gl.NEAREST,
            minFilter: game.renderer.gl.NEAREST,
        };
        this.uniforms.iChannel3.value = game.level.lightTexture.shadowDrawer.sprite.texture;
        this.uniforms.iChannel3.textureData = undefined;

        this.uniforms.camera = {value: {x:0, y:0}, type: '2f'};

        this.fragmentSrc = `
        precision mediump float;
        
        varying vec2       vTextureCoord;
        uniform vec2       offset;
        uniform float      time;
        uniform sampler2D  uSampler;
        uniform sampler2D  iChannel0;
        uniform sampler2D  iChannel1;
        uniform sampler2D  iChannel2;
        uniform sampler2D  iChannel3;
        uniform vec2       resolution;
        uniform vec2       camera;
        
        uniform vec4       lights[20];
        uniform int        lightsCount;
        uniform vec4       noises[20];
        uniform int        noisesCount;
        
        #define WIDTH ${game.world.width}.
        #define HEIGHT ${game.world.height}.

        #define CWIDTH ${game.level.lightTexture.sprite.width}.
        #define CHEIGHT ${game.level.lightTexture.sprite.height}.

        #define SWIDTH ${game.level.lightTexture.shadowDrawer.sprite.width*SHADOW_ZM}.
        #define SHEIGHT ${game.level.lightTexture.shadowDrawer.sprite.height*SHADOW_ZM}.
        
        #define FOG_OF_WAR vec4(0, 0, 0, 1.)
        #define FLASHLIGHT vec4(255./255., 241./255., 224./255., 1.)
        #define RADIUS0 ${BIG_LIGHT_RADIUS}.
        #define RADIUS1 ${LIGHT_RADIUS}.
        #define RADIUS_TREMBLE ${LIGHT_TREMBLE}.
        #define RADIUS2 ${DYING_RADIUS}.
        
        #define GLOW 8.
        
        //X = x - camera.x      =>  x = camera.x + X
        //Y = camera.y + camera.height - y              => y = camera.y + camera.height - Y
        vec4 getLightPix() {
            return texture2D(iChannel2, vec2((camera.x + gl_FragCoord.x)/CWIDTH, (camera.y - gl_FragCoord.y)/CHEIGHT));
        }
        
        vec4 getShadowPix() {
            return texture2D(iChannel3,  vec2((camera.x + gl_FragCoord.x)/SWIDTH, (camera.y - gl_FragCoord.y)/SHEIGHT));
        }
        
        
        float lightRadius(float time) {
            float amplitude = max(0., (1000. - time)/1000.);
            return mix(RADIUS2, RADIUS1, abs(cos(time/100.))*amplitude);
        }
        
        float lightStrength(float time) {
            float amplitude = max(0., (1000. - time)/1000.);
            return abs(cos(time/100.))*amplitude;
        }
        
        float rand(vec2 co){
            return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
        }
        
        float randSin(vec2 co, float tm) {
            return sin(time*tm + rand(co))*0.5 + 0.5;
        }
        
        vec2 getDynamicLight() {
            float lightness = 0.;
            float glow = 0.;
            for (int i = 0; i < 20; i++) {
                if (i >= lightsCount) break;
                vec2 xy = lights[i].xy;
                float radius = lightRadius(lights[i].z);
                
                float dx = randSin(xy,.1)*RADIUS_TREMBLE;
                float dy = randSin(xy,-.2)*RADIUS_TREMBLE;
                float dr = randSin(xy,4.)*RADIUS_TREMBLE;
                
                xy = xy + vec2(dx,dy);
                radius += dr;
                float st = lightStrength(lights[i].z);
                
                float dist = distance(xy, gl_FragCoord.xy);
                float thisLight = step(dist, radius) * smoothstep(0., 1., 1. - pow(dist/radius, 8.))*st*0.4;
                thisLight += step(dist, RADIUS0)*smoothstep(0., 1., 1. - dist/RADIUS0)*0.05*st;
                lightness = max(0., min(1., max(lightness + thisLight, lightness)));
                //glow = max(glow, step(dist, 3.)*GLOW);
            }
            return vec2(lightness + glow, glow);
        }
        
        vec2 getStaticLight() {
            vec4 sl = getLightPix();
            //vec4 glowColor = vec4(sl.r, sl.g, 0., sl.a);
            float glow = max(sl.r, sl.g)*GLOW*sl.a;
            float light = smoothstep(0., 1., 1. - pow(1. - sl.a, 3.))*sl.b;
            return vec2(max(glow, light), glow);
        }
        
        vec4 applyLight(vec4 color, float lightness) {
            color = mix(FOG_OF_WAR, mix(color, color + FLASHLIGHT*0.01, lightness), min(1., lightness+0.2));
            //glow if lightness > 1.
            color = color * max(1., lightness);
            return color;
        }
        
        vec4 blur5(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
            vec4 color = vec4(0.0);
            vec2 off1 = vec2(1.3333333333333333) * direction;
            color += texture2D(image, uv) * 0.29411764705882354;
            color += texture2D(image, uv + (off1 / resolution)) * 0.35294117647058826;
            color += texture2D(image, uv - (off1 / resolution)) * 0.35294117647058826;
            return color; 
        }
        
        //return 0-1, 1 - max shadowing
        float getShadowFactor() {
            vec4 color = blur5(iChannel3, 
                vec2((camera.x + gl_FragCoord.x)/SWIDTH, (camera.y - gl_FragCoord.y)/SHEIGHT), 
                resolution, 
                vec2(1.,  0.3)
            );
            //color = getShadowPix();
            return pow(color.b*color.a, 0.5);
        }
        
        vec2 calculateLight() {
            return max(getStaticLight(), getDynamicLight());
        }
        
        vec4 getMonsterPix() {
            return texture2D(iChannel0, vec2(vTextureCoord.x, 1. - vTextureCoord.y));
        }   

        vec4 getMonsterBlur(float lightness) {
            float blur = pow(max(pow(1. - lightness, 0.1), cos(time*0.1)*0.5+0.5),10.);
            return blur5(iChannel0, vec2(vTextureCoord.x, 1. - vTextureCoord.y), resolution, blur*(vec2(rand(vTextureCoord), rand(vTextureCoord.yx))));
        }   
        
        vec4 applyMonsters(vec4 color, float lightness, float glow) {
            vec4 monsterColor = getMonsterPix();
            vec4 monsterBlur = getMonsterBlur(lightness);
            vec3 mappedColor = monsterBlur.rgb * (1. + ceil(monsterBlur.r)*GLOW) * vec3(2., 0.1, 0.);
            
            mappedColor = mix(mappedColor, vec3(0,0,0), 1. - step(glow, 0.)); //if glow - just show black
            
            monsterBlur.rgb = mappedColor.rgb;

            color = mix(color, monsterBlur, monsterBlur.a*smoothstep(0., 0.1, lightness));
           
            return color;
        }
        
        
        #define NOISE_RADIUS_1 ${PIX*2}.
        #define NOISE_AB ${12}.
        #define NOISE_BC ${6}.
        #define NOISE_CD ${2}.
        #define NOISE_OFFSET 4.
        
        vec4 getNoiseOffsetAndColorMod() {
            vec4 res = vec4(0, 0, 0, 1);
            //float total = 0.;
            for (int i = 0; i < 20; i++) {
                if (i >= noisesCount) break;
                vec2 xy = noises[i].xy;
                float radius = NOISE_RADIUS_1;
                
                float dist = distance(xy, gl_FragCoord.xy);
                
                float factor = pow(noises[i].z, 0.4);
                float startMod = noises[i].w;
                float bMod = noises[i].w + (NOISE_AB)*factor/radius;
                float cMod = noises[i].w + (NOISE_AB + NOISE_BC)*factor/radius;
                float endMod = noises[i].w + (NOISE_AB + NOISE_BC + NOISE_CD)*factor/radius;
                
                float part = dist / radius;
                
                vec2 pixOffset = -(gl_FragCoord.xy - xy)/resolution/dist/2.;
                
                float decay = 1. - noises[i].w;
                
                if (part >= startMod && part < bMod) {
                    res.xy += pixOffset*mix(0., NOISE_OFFSET, smoothstep(startMod, bMod, part))*decay;
                    res.a += smoothstep(1., 2.,smoothstep(startMod, bMod, part))*decay;
                } else if (part >= bMod && part < cMod) {
                    res.xy += pixOffset*NOISE_OFFSET*decay;
                    res.a += 1.*decay;
                } else if (part >= cMod && part <= endMod) {
                    res.xy += pixOffset*mix(NOISE_OFFSET, 0., smoothstep(cMod, endMod, part))*decay;
                    res.a += smoothstep(0.5, 1., smoothstep(cMod, endMod, part))*decay;
                }
            }
            res.x = sign(res.x)*pow(abs(res.x), 1.2);
            res.y = sign(res.y)*pow(abs(res.y), 1.2);
            return res;
        }
        
        vec4 addNoiseColor(vec4 color) {
            vec4 outColor = color;
            for (int i = 0; i < 20; i++) {
                if (i >= noisesCount) break;
                vec2 xy = noises[i].xy;
                float radius = noises[i].z;
                
                float dist = distance(xy, gl_FragCoord.xy);
                outColor = mix(outColor, vec4(0,0,1,1), step(dist, radius)*noises[i].w);
            }
            return outColor;
        }
        
        vec4 hdr(vec4 color) {
            //hdr algorithm
            const float gamma = 2.2;
            // тональная компрессия
            vec3 mapped = color.rgb / (color.rgb + vec3(1.0));
            // гамма-коррекция
            //mapped = pow(mapped, vec3(1.0 / gamma));
            
            return vec4(mapped, color.a);
        }
        
        void main() {
            float isUi = ceil(texture2D(iChannel1, vec2(vTextureCoord.x, 1. - vTextureCoord.y)).a);
            if (isUi > 0.) {
                gl_FragColor = texture2D(uSampler, vTextureCoord);    
            } else {
                vec4 noiseOffsetAndColor = getNoiseOffsetAndColorMod();
                vec4 color = texture2D(uSampler, vTextureCoord + noiseOffsetAndColor.xy);
                color *= pow(noiseOffsetAndColor.a, 0.2);
                
                vec2 lng = calculateLight();   
                float lightness = lng.x;
                float glow = lng.y;
                
                float shadow = getShadowFactor();
                float lightnessToDraw = max(lightness - shadow, 0.);
    
                vec4 gameColor = applyMonsters(color, lightness, glow);
                gameColor = applyLight(gameColor, lightnessToDraw);
                gl_FragColor = hdr(gameColor);
                ${game.DEBUG_LIGHT ? "gl_FragColor = getLightPix() + getShadowPix()" : ""};
                
                //gl_FragColor.r = glow;
                //gl_FragColor = getShadowPix();
                //gl_FragColor.r = isUi;
            }    
        }
        `;

        this.noisesToShow = [];
        game.level.noise.state.onChange(s => s.emittedNoises, noises => {
            if (noises.length) {
                this.noisesToShow = this.noisesToShow.concat(noises.map(noise => ({
                    noise, timeout: 500
                })));
                while (this.noisesToShow.length > 20) this.noisesToShow.shift();
            }
        })
    }

    update() {
        super.update();
        let units = game.level.state.objects.unit;
        this.uniforms.lightsCount.value = units.length;
        let i = 0;
        for (let unitState of units) {
            this.uniforms.lights.value[i++] = unitState.light.x - game.camera.x;
            this.uniforms.lights.value[i++] = game.camera.y + game.camera.height - unitState.light.y;
            this.uniforms.lights.value[i++] = unitState.light.dyingTime;
            i++;
        }

        i = 0;
        let noises = this.noisesToShow.slice();
        this.noisesToShow = [];
        for (let n of noises) {
            n.timeout -= game.time.physicsElapsedMS;
            if (n.timeout >= 0) {
                this.noisesToShow.push(n);
                this.uniforms.noises.value[i++] = n.noise.x - game.camera.x;
                this.uniforms.noises.value[i++] = game.camera.y + game.camera.height - n.noise.y;
                this.uniforms.noises.value[i++] = n.noise.loudness;
                this.uniforms.noises.value[i++] = (500-n.timeout)/500;
            }
        }
        this.uniforms.camera.value = {
            x: game.camera.x,
            y: game.camera.y + game.camera.height
        };
        this.uniforms.noisesCount.value = this.noisesToShow.length;

    }
}
