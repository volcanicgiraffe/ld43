export const SELECTION_YELLOW = {r: 255, g: 255, b: 0};
export const SELECTION_RED = {r: 255, g: 0, b: 0};
export const SELECTION_BLUE = {r: 0, g: 0, b: 255};
export const SELECTION_GREEN = {r: 0, g: 255, b: 0};

export default class SelectionShader extends Phaser.Filter {
    constructor(rgb = SELECTION_YELLOW) {
        super(game);
        this.uniforms.selectionColor = {type: '4f', value: {x:rgb.r, y:rgb.g, z:rgb.b, w:255}};
        this.uniforms.blinking = {type: '1f', value: 0};

        this.fragmentSrc = `
        precision mediump float;
        
        varying vec2       vTextureCoord;
        uniform vec2       offset;
        uniform float      time;
        uniform float      blinking;
        uniform sampler2D  uSampler;
        uniform vec2       resolution;
        uniform vec4       selectionColor;
        
        #define BLINK_SPEED 4.
        
        #define OFF(dx,dy) (vTextureCoord + vec2(dx,dy)/vec2(1024, 1024))
        
        void main(void) {
        
            vec4 clr = texture2D(uSampler, vTextureCoord);
            float thisAlpha = clr.a;
            
            float around = texture2D(uSampler, OFF(-1,-1)).a
                         + texture2D(uSampler, OFF(-1,0)).a  
                         + texture2D(uSampler, OFF(-1,1)).a  
                         + texture2D(uSampler, OFF(0,-1)).a  
                         + texture2D(uSampler, OFF(0,1)).a  
                         + texture2D(uSampler, OFF(1,-1)).a  
                         + texture2D(uSampler, OFF(1,0)).a  
                         + texture2D(uSampler, OFF(1,1)).a;
                         
            float showSelection = clamp(around / 3., 0., 1.) * (1. - thisAlpha) * (1. - blinking*(sin(time*BLINK_SPEED)*0.5 + 0.5));             
                         
            gl_FragColor = mix(clr, selectionColor / 255., showSelection);
        }
    
    `;

    }

    setColor(rgb) {
        this.uniforms.selectionColor.value = {x:rgb.r, y:rgb.g, z:rgb.b, w:255};

    }
}

export function showSelection(sprite, color, blinking = 0, doShow = true) {
    if (doShow === true && !sprite.filters) {
        sprite.filters = [new SelectionShader(color)];
    }
    if (doShow === true) {
        sprite.filters[0].uniforms.blinking.value = blinking;
        sprite.filters[0].setColor(color);
        sprite.filters[0].update();
    }
    if (doShow === false && sprite.filters) {
        sprite.filters = undefined;
    }
}

export function hideSelection(sprite) {
    sprite.filters = undefined;
}

