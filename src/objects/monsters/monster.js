import { StateBase } from "../../utils/state";
import StatefulSprite from "../StatefulSprite";
import {
    LOUD_MONSTER_LIMIT,
    LOUD_KICK,
    NOISE_ATTENTION,
    NOISE_HUMAN,
    NOISE_MECHANICAL
} from "../sound/NoiseManager";
import Mover from "../mover";
import Unit from "../units/unit";
import { PIX } from "../../consts"
import Alert, {AlertShower} from "../../helpers/alerts";

const IDLE = "idle";

const HUNTING = "hunting";
const LISTENING = "listening";

const EATING_PHASE_START = "eating-start";
const EATING_PHASE = "eating";
const EATING_PHASE_STOP = "eating-stop";

const BREAKING_DOOR = "breaking-door";

class MonsterState extends StateBase {
    constructor(x, y) {
        super(undefined, "monster");

        this.x = x;
        this.y = y;
        this.substate = IDLE;
        this.eatingOf = null;
        this.eatingTimeout = 0;
        this.listeningTimeout = 0;
        this.lastLoudNoise = undefined;
        this.lastVisibleUnit = undefined;

        this.doorToBreak = undefined;
        this.doorKickTimeout = undefined;
        this.doorKickingAnimation = undefined;

        this.movement = {
            moving: false,
            doorOnWay: undefined,
            velocity: { x: 0, y: 0 },
            path: [],
            target: undefined
        };

        this.huntingTarget = undefined;
        this.visionDelay = 0;

        this.postConstruct().save();
    }
}

const VisionDelay = 200; //allow monster to walk a bit before changing target

class DarkParticle extends Phaser.Sprite {
    constructor(game, x, y) {
        super(game, x, y, 'enemy', 'p1_1.png');
        let anim = this.animations.add('idle', ['p1_1.png', 'p1_2.png', 'p1_3.png', 'p1_4.png', 'p1_5.png'], 14);
        anim.killOnComplete = true;
    }
}

const LISTENING_PERIOD = 1000;


class Leg {
    constructor(body, angle) {
        this.body = body;
        this.angle = angle;

        let radius = this.radius = 18;
        let length = this.length = 40;
        let angleDiff = this.angleDiff = angle / 30;
        this.model = {
            x1: this.body.parent.x + Math.cos(angle) * radius,
            y1: this.body.parent.x + Math.sin(angle) * radius,
            x15: this.body.parent.x + Math.cos(angle - angleDiff) * (radius + length / 2),
            y15: this.body.parent.y + Math.sin(angle - angleDiff) * (radius + length / 2),
            x2: this.body.parent.x + Math.cos(angle + angleDiff) * (radius + length),
            y2: this.body.parent.y + Math.sin(angle + angleDiff) * (radius + length),

            z: 0
        };
        this.segment1 = [];
        this.segment2 = [];
        for (let i = 0; i < 3; i++) {
            let seg = game.add.sprite(
                this.model.x1 + (this.model.x15 - this.model.x1) * (i + 0.5) / 3,
                this.model.y1 + (this.model.y15 - this.model.y1) * (i + 0.5) / 3,
                "enemy",
                "leg1.png"
            );
            seg.i = i;
            seg.anchor.set(0.5, 0.5);
            this.segment1.push(seg);
            this.body.addChild(seg);
        }
        for (let i = 0; i < 3; i++) {
            let seg = game.add.sprite(
                this.model.x15 + (this.model.x2 - this.model.x15) * (i + 0.5) / 3,
                this.model.y15 + (this.model.y2 - this.model.y15) * (i + 0.5) / 3,
                "enemy",
                "leg2.png"
            );
            seg.i = i;
            seg.anchor.set(0.5, 0.5);
            this.segment2.push(seg);
            this.body.addChild(seg);
        }
        this.redraw();
    }

    redraw() {
        this.model.x1 = this.body.parent.x + Math.cos(this.angle) * this.radius;
        this.model.y1 = this.body.parent.y + Math.sin(this.angle) * this.radius;
        for (let s of this.segment1) {
            s.x = this.model.x1 + (this.model.x15 - this.model.x1) * (s.i + 0.5) / 3 - this.body.parent.x;
            s.y = this.model.y1 + (this.model.y15 - this.model.y1) * (s.i + 0.5) / 3 - this.body.parent.y;
        }
        for (let s of this.segment2) {
            s.x = this.model.x15 + (this.model.x2 - this.model.x15) * (s.i + 0.5) / 3 - this.body.parent.x;
            s.y = this.model.y15 + (this.model.y2 - this.model.y15) * (s.i + 0.5) / 3 - this.body.parent.y;
        }
    }

    getTheoreticalX2Y2() {
        return {
            x2: this.body.parent.x + Math.cos(this.angle + this.angleDiff) * (this.radius + length),
            y2: this.body.parent.y + Math.sin(this.angle + this.angleDiff) * (this.radius + length)
        }
    }

    updateMovement(movement, progress) {
        let { x, y, fromX, fromY } = movement;
        if (movement.toIdle) {
            x = this.body.parent.x + Math.cos(this.angle + this.angleDiff) * (this.radius + this.length);
            y = this.body.parent.y + Math.sin(this.angle + this.angleDiff) * (this.radius + this.length);
        }
        this.model.x2 = fromX + (x - fromX) * progress;
        this.model.y2 = fromY + (y - fromY) * progress;

        this.model.z = 1 - Math.abs(progress-0.5);

        this.model.x15 = this.body.parent.x + Math.cos(this.angle - this.angleDiff) * (this.radius + this.length / 2);
        this.model.y15 = this.body.parent.y + Math.sin(this.angle - this.angleDiff) * (this.radius + this.length / 2);
    }
}

const LEG_STEP = PIX;

class MonsterMovements {

    constructor(sprite) {
        this.sprite = sprite;
        this.body = game.add.sprite(0, 0, "enemy", "body.png");
        this.body.anchor.set(0.5, 0.5);

        //todo: glow eyes
        sprite.addChild(this.body);

        this.legs = [
            new Leg(this.body, -Math.PI / 2),
            new Leg(this.body, 2 * Math.PI / 3 - Math.PI / 2),
            new Leg(this.body, -2 * Math.PI / 3 - Math.PI / 2),
        ];

        let eye1 = game.add.sprite(10, -3, "enemy", "eye_b.png");
        let eye2 = game.add.sprite(-5, 4, "enemy", "eye_b.png");
        this.body.addChild(eye1);
        this.body.addChild(eye2);
        this.setEyeTween(eye1, this.sprite);
        this.setEyeTween(eye2, this.sprite);
        let eye1s = game.add.sprite(-15, -8, "enemy", "eye_s.png");
        let eye2s = game.add.sprite(6, -15, "enemy", "eye_s.png");
        this.setEyeTween(eye1s, this.sprite);
        this.setEyeTween(eye2s, this.sprite);
        this.body.addChild(eye1s);
        this.body.addChild(eye2s);

        this.legToStep = 0;
        this.legMovement = null;
    }

    setEyeTween(eye, parent) {
        const agitated = parent.state && (parent.state.substate === HUNTING || parent.state.substate === EATING_PHASE);
        const minSpeed = agitated ? 100 : 400;
        const maxSpeed = agitated ? 400 : 3000;
        const minDelay = agitated ? 100 : 400;
        const maxDelay = agitated ? 1000 : 12000;
        game.add.tween(eye).to({ x: game.rnd.integerInRange(-15, 10), y: game.rnd.integerInRange(-15, 10) },
            game.rnd.integerInRange(minSpeed, maxSpeed), Phaser.Easing.Linear.InOut, true, game.rnd.integerInRange(minDelay, maxDelay))
            .onComplete.addOnce(() => {
                if (parent.alive) {
                    this.setEyeTween(eye, parent);
                }
            });
    }

    onMove(vx, vy) {
        if (vx === 0 && vy === 0) return this.onStopped();
        //vx,vy -> velocity
        if (!this.legMovement) {
            let leg = this.legs[this.legToStep];

            //leg moves forward for LEG_STEP
            let direction = new Phaser.Point(vx, vy);
            let vel = direction.getMagnitude();
            direction.setMagnitude(LEG_STEP);

            let { x2: fromX, y2: fromY } = leg.getTheoreticalX2Y2();

            let targetX = fromX + direction.x;
            let targetY = fromY + direction.y;
            let timeToExecute = LEG_STEP / vel / 2;
            this.legMovement = {
                leg,
                fromX, fromY,
                x: targetX,
                y: targetY,
                timeTotal: timeToExecute,
                timePassed: 0
            };
        }

    }

    onStopped() {
        if (this.legMovement) {
            this.legMovement.toIdle = true;
        }
    }

    /*

    move body to new (x,y) ->
        move body + step 1 leg
     */

    update() {
        if (this.legMovement) {
            this.legMovement.timePassed += game.time.physicsElapsedMS;
            let progress = Math.min(1, this.legMovement.timePassed / this.legMovement.timeTotal);
            this.legMovement.leg.updateMovement(this.legMovement, progress);
            //console.log(JSON.stringify(this.legMovement.leg.model));
            if (progress === 1) {
                game.sounder.play('step_a');
                this.legMovement = null;
                this.legToStep = (this.legToStep + 1) % 3;
            }
        }
        for (let leg of this.legs) {
            leg.redraw();
        }
    }

}

const DOOR_KICK_TIMEOUT = 1000;

export default class Monster extends StatefulSprite {

    constructor(x, y) {
        super(game, x, y, "test", 8);
        this.animations.add(EATING_PHASE_START, [9, 10], 16);
        this.animations.add(EATING_PHASE, [11, 12], 16, true);
        this.animations.add(EATING_PHASE_STOP, [10, 9, 8], 16);
        this.pem = game.add.emitter(this.x, this.y);
        this.pem.particleClass = DarkParticle;
        this.pem.start(false, 3000, 50);
        this.anchor.set(0.5, 0.5);

        this.monsterParts = new MonsterMovements(this);
        this.setState(new MonsterState(x, y));


        this.state.onChange(n => n.substate, newState => {
            this.state.huntingTarget = undefined;
            switch (newState) {
                case EATING_PHASE_START:
                    this.mover.stop();
                    this.animations.play(EATING_PHASE_START).onComplete.addOnce(() => this.state.substate = EATING_PHASE);
                    break;
                case EATING_PHASE:
                    this.animations.play(EATING_PHASE);
                    this.state.eatingTimeout = 3000;
                    break;
                case EATING_PHASE_STOP:
                    this.animations.play(EATING_PHASE_STOP).onComplete.addOnce(() => this.state.substate = IDLE);
                    break;
                case HUNTING:
                    game.sounder.play('roar');
                    //play move
                    break;
                case LISTENING:
                    this.mover.stop();
                    //play thinking
                    this.state.listeningTimeout = LISTENING_PERIOD;
                    break;
                case IDLE:
                    this.mover.stop();
                    break;
                case BREAKING_DOOR:
                    this.mover.stop();
                    this.startBreakingDoor();
                    break;
            }
        });
        this.loudListener = game.level.noise.addListener(this, LOUD_MONSTER_LIMIT);

        this.mover = new Mover(this, game.level.passMap.findMonsterPath.bind(game.level.passMap));

        this.state.onChange(s => s.lastVisibleUnit, visible => {
            if (visible) {
                switch (this.state.substate) {
                    case IDLE:
                        this.state.substate = HUNTING;
                        break;
                    case LISTENING:
                        this.state.substate = HUNTING;
                        break;
                    case HUNTING:
                        this.state.huntingTarget = undefined;
                        break;
                }
            }
        });
        this.state.onChange(s => s.lastLoudNoise, noise => {
            switch (this.state.substate) {
                case IDLE:
                    if (noise) {
                        // console.log("humans?");
                        this.state.substate = LISTENING;
                    }
                    break;
                case LISTENING:
                    if (noise) {
                        // console.log("humannnzz!!!");
                        this.state.substate = HUNTING;
                    } else {
                        // console.log("no humannnzz :(");
                        this.state.substate = IDLE;
                    }
                    break;
            }
        });

        this.state.onChange(s => s.huntingTarget, target => {
            if (this.state.substate === HUNTING && target) {
                Unit.unitMoveTo(this.state, target);
            }
        });
        this.state.onChange(s => s.movement.moving, moving => {
            if (moving === false && this.state.substate === HUNTING) {
                //reset visible state, reset listened state
                this.state.lastVisibleUnit = undefined;
                this.state.lastLoudNoise = undefined;
                this.state.huntingTarget = undefined;
                this.state.substate = LISTENING;
            }
        });

        this.alertShower = new AlertShower();

    }

    startBreakingDoor() {
        //1. jump to point before door
        let pointToJump = this.state.doorToBreak.getMiddlePointBefore(this);
        //todo: jump animation
        this.monsterParts.onMove(pointToJump.x - this.state.x, pointToJump.y - this.state.y);
        this.state.x = pointToJump.x;
        this.state.y = pointToJump.y;
        this.state.doorKickTimeout = DOOR_KICK_TIMEOUT;
    }

    continueBreakingDoor() {
        if (this.state.doorToBreak.state.opened) {
            //no need to break!
            Monster.cancelBreakDoor(this.state);
            //stop animations
            this.state.doorKickingAnimation = false;
            return;
        }

        if (this.state.doorKickingAnimation) {

        } else {
            this.state.doorKickTimeout -= game.time.physicsElapsedMS;
            if (this.state.doorKickTimeout < 0) {
                this.state.doorKickingAnimation = true;
                //todo: kick animation
                game.add.tween(this.state.doorToBreak).to({ alpha: 0.4 }, 100, null, true).onComplete.addOnce(() => {
                    if (!this.state.doorToBreak) return;
                    game.add.tween(this.state.doorToBreak).to({ alpha: 1 }, 100, null, true).onComplete.addOnce(() => {
                        if (!this.state.doorToBreak) return;
                        game.sounder.play('bang');
                        game.level.noise.emitNoise(this.state.doorToBreak.colBox.centerX, this.state.doorToBreak.colBox.centerY, NOISE_MECHANICAL, LOUD_KICK);
                        this.state.doorToBreak.damageDoor(10);
                        this.state.doorKickingAnimation = false;
                        this.state.doorKickTimeout = DOOR_KICK_TIMEOUT;
                    })
                })
            }

        }


    }

    interestingSound(sound) {
        return sound && (sound.type === NOISE_HUMAN || sound.type === NOISE_ATTENTION);
    }

    listen() {
        let mostNoise = this.loudListener.getMostLoudNoise();
        this.state.lastLoudNoise = this.interestingSound(mostNoise) ? mostNoise : undefined;
    }

    update() {
        super.update();
        this.alertShower.update();
        if (this.state.movement.moving) {
            this.monsterParts.onMove(this.state.movement.velocity.x, this.state.movement.velocity.y);
        } else {
            this.monsterParts.onStopped();
        }
        this.monsterParts.update();

        this.pem.x = this.x;
        this.pem.y = this.y;


        if (this.state.lastVisibleUnit && !this.state.lastVisibleUnit.sprite.alive) {
            this.state.lastVisibleUnit = undefined;
        }
        this.loudListener.state.checkChanges();

        if (this.state.substate === EATING_PHASE) {
            this.state.eatingTimeout -= game.time.physicsElapsedMS;
            if (this.state.eatingTimeout < 0) {
                this.state.substate = EATING_PHASE_STOP;
                this.state.eatingOf.died = true;
                this.state.eatingOf = null;
            }
        }
        if (this.state.substate === IDLE || this.state.substate === LISTENING || this.state.substate === HUNTING) {
            if (this.state.lastVisibleUnit) {
                this.state.substate = HUNTING;
            }
            this.state.listeningTimeout -= game.time.physicsElapsedMS;
            if (this.state.listeningTimeout < 0) {
                this.listen();
                this.state.listeningTimeout = LISTENING_PERIOD;
            }
        }

        if (this.state.substate === HUNTING) {
            this.hunt();
            this.checkDoorsOnWay();
        }

        if (this.state.substate === BREAKING_DOOR) {
            this.continueBreakingDoor();
        }

        this.x = this.state.x;
        this.y = this.state.y;
    }

    checkDoorsOnWay() {
        let doorOnWay = this.state.movement.doorOnWay;
        if (doorOnWay && Phaser.Point.distance(this, { x: doorOnWay.centerX, y: doorOnWay.centerY }) < 3 * PIX) {
            let minDistance = Math.min(
                Math.abs(this.x - doorOnWay.left),
                Math.abs(this.x - doorOnWay.right),
                Math.abs(this.y - doorOnWay.top),
                Math.abs(this.y - doorOnWay.bottom)
            );
            if (minDistance < 3 * PIX && !doorOnWay.state.opened) {
                // console.log("closed door is near!");
                Monster.doBreakDoor(this.state, doorOnWay);
            }
        }
    }

    static doBreakDoor(monsterState, doorSprite) {
        Unit.unitStop(monsterState);
        monsterState.substate = BREAKING_DOOR;
        monsterState.doorToBreak = doorSprite;
    }

    static cancelBreakDoor(monsterState) {
        monsterState.doorToBreak = undefined;
        monsterState.substate = IDLE;
    }

    hunt() {
        if (this.state.visionDelay > 0) this.state.visionDelay -= game.time.physicsElapsedMS;
        if (this.state.huntingTarget && !this.state.movement.target) {
            this.state.huntingTarget = undefined;
        }
        //return;
        if (this.state.huntingTarget && this.state.lastLoudNoise && this.state.huntingTarget.noise) {
            if (this.state.huntingTarget.noise.loudness < this.state.lastLoudNoise.loudness) {
                this.state.huntingTarget = undefined;
            }
        }

        if (this.state.lastVisibleUnit) {
            this.state.huntingTarget = {
                visible: this.state.lastVisibleUnit,
                x: this.state.lastVisibleUnit.x,
                y: this.state.lastVisibleUnit.y
            }
            if (this.state.lastVisibleUnit.sprite !== this.state.lastTarget) {
                this.alertShower.showAlert(this.state.lastVisibleUnit.x, this.state.lastVisibleUnit.y, 'm_alert');
                this.state.lastTarget = this.state.lastVisibleUnit.sprite;
            }
        } else if (!this.state.huntingTarget && this.state.lastLoudNoise) {
            let dist = Math.hypot(this.state.lastLoudNoise.x - this.x, this.state.lastLoudNoise.y - this.y);
            let point = game.level.passMap.getPassablePointInRadius(
                this.state.lastLoudNoise.x,
                this.state.lastLoudNoise.y,
                dist / 4,
                dist / 2
            );
            if (point) {
                this.state.huntingTarget = {
                    noise: Object.assign({}, this.state.lastLoudNoise),
                    x: point.x,
                    y: point.y
                };
                if (this.state.huntingTarget !== this.state.lastTarget) {
                    this.alertShower.showAlert(this.state.huntingTarget.noise.x, this.state.huntingTarget.noise.y, 'm_warn');
                    this.state.lastTarget = this.state.huntingTarget.noise;
                }
            }
        }


        this.mover.updateMovement();

    }

    sees(unitSprite) {
        if (this.state.visionDelay > 0) return;
        if (!this.state.lastVisibleUnit || Phaser.Point.distance(this, this.state.lastVisibleUnit) > Phaser.Point.distance(this, unitSprite) || unitSprite === this.state.lastVisibleUnit.sprite) {
            this.state.lastVisibleUnit = { sprite: unitSprite, x: unitSprite.x, y: unitSprite.y };
            this.state.visionDelay = VisionDelay;
        }
    }

    canGrab(unitSprite) {
        return unitSprite.canBeGrabbed && Phaser.Point.distance(unitSprite.state, this.state) < PIX;
    }

    eatUnit(unitSprite) {
        if (this.state.substate !== IDLE && this.state.substate !== LISTENING && this.state.substate !== HUNTING) return;
        this.state.substate = EATING_PHASE_START;
        this.state.eatingOf = unitSprite.state;

        unitSprite.setBeingEated(this);
    }

    get pixPerMs() {
        return this.state.lastVisibleUnit ? 0.3 : 0.2;
    }

}
