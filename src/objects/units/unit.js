import { StateBase, UNCHANGED } from "../../utils/state";
import StatefulSprite from "../StatefulSprite";
import {
    LOUD_ATTENTION,
    LOUD_SCREAM,
    LOUD_SNEEZE,
    LOUD_STEP,
    NOISE_ATTENTION,
    NOISE_HUMAN
} from "../sound/NoiseManager";
import Mover from "../mover";
import { PIX } from "../../consts";
import { SELECTION_GREEN, SELECTION_RED, showSelection } from "../../effects/selection";
import { createProfile } from "./registry";

export const STATE_NORMAL = "normal";  //unit can execute orders
export const STATE_PANIC = "panic";    //unit panics
export const STATE_DYING = "dying";    //unit dying
export const STATE_EVACUATED = "evacuated";

const SUBSTATE_IDLE = "idle";
const SUBSTATE_MOVING = "moving";
export const SUBSTATE_NOISING = "noising";
const FULL_SPEED = 0.1;
const PANIC_BOOST = 2;

const UnitTypes = ['red', 'yellow', 'blue'];


const TimeBetweenRandomSoundsRange = [20000, 40000];
const RandomSoundChancePerNeighbours = {
    0: 0.05,
    1: 0.1,
    2: 0.2,
    3: 0.3,
    4: 0.4,
    5: 0.5,
    6: 0.5,
    7: 0.5,
    8: 0.5,
    9: 0.5,
    10: 0.5
};

const TimeBetweenIdleRotation = [1500, 12000];

const NoiseEach = 500;
const PanicTimeout = 5000;

class UnitState extends StateBase {
    constructor(x, y, profile) {
        super(undefined, "unit");
        this.x = x;
        this.y = y;
        this.rotation = 0;
        this.profile = profile;
        if (!profile || !profile.type) {
            this.profile = createProfile();
        }

        this.movement = {
            moving: false,
            doorOnWay: undefined,
            velocity: { x: 0, y: 0 },
            path: [],
            target: undefined //{x: 0, y: 0}
        };

        this.light = {
            x: x,
            y: y,
            dyingTime: 0
        };

        this.selected = false;

        this.noiseRequested = false;
        this.timeToNoise = 0;

        this.mainState = STATE_NORMAL;
        this.substate = SUBSTATE_IDLE;
        this.died = false;

        this.allowRandomNoise = 0;
        this.timeToRandomSound = game.rnd.integerInRange(...TimeBetweenRandomSoundsRange);
        this.neighbours = 0;

        this.seesMonsterRightNow = false;
        this.panicTimeout = 0;

        this.postConstruct().save();
    }
}

export default class Unit extends StatefulSprite {

    constructor(x, y, profile) {
        super(game, x, y, "human", "blue_idle.png");
        this.setState(new UnitState(x, y, profile));
        const type = this.state.profile.type;
        this.animations.add("idle", [type + '_idle.png'], 16, false).play();
        this.animations.add("walking", Phaser.Animation.generateFrameNames(type + '_w', 1, 8, '.png'), 10, true);
        this.animations.add("die", Phaser.Animation.generateFrameNames(type + '_dying', 1, 4, '.png'), 12);
        this.animations.add("die2", Phaser.Animation.generateFrameNames(type + '_d2_', 1, 6, '.png'), 12);

        this.animations.add("noising", Phaser.Animation.generateFrameNames(type + '_dying', 1, 2, '.png').concat(Phaser.Animation.generateFrameNames(type + '_dying', 2, 1, '.png')), 12);
        this.walkSound = game.add.sound('footsteps');
        this.anchor.set(0.5);

        this.state.onChange(p => p.movement.moving, newMoving => {
            if (this.state.mainState === STATE_DYING) return;
            this.animations.play(newMoving ? "walking" : "idle");
            if (newMoving) {
                this.walkSound.play(undefined, Math.random()*5, 0.8, true, false);
            } else {
                this.walkSound.stop();
            }
            if (this.state.mainState === STATE_NORMAL) {
                switch (this.state.substate) {
                    case SUBSTATE_NOISING:
                        if (newMoving) this.state.substate = SUBSTATE_MOVING; //ignore stop event
                        break;
                    case SUBSTATE_MOVING:
                    case SUBSTATE_IDLE:
                        this.state.substate = newMoving ? SUBSTATE_MOVING : SUBSTATE_IDLE;
                        break;
                }
            }
        });

        this.state.onEvent("noiseRequested", () => {
            if (this.state.mainState === STATE_NORMAL) {
                this.stop();
                game.sounder.play('loudnoise');
                this.state.substate = SUBSTATE_NOISING;
            }
        });

        this.state.onChange(p => p.died, () => {
            this._flash.destroy();
            this.destroy();
        });
        this.state.onChange(p => p.mainState, newMainState => {
            game.level.state.global.anyStatusChanged = true;
            this._panicIcon.visible = false;
            this.updateSelection();
            switch (newMainState) {
                case STATE_EVACUATED:
                    game.level.state.global.evacuatedUnits = game.level.state.global.evacuatedUnits.concat(this.state.id);
                    this.destroy();
                    break;
                case STATE_NORMAL:
                    this.animations.play("idle");
                    break;
                case STATE_DYING:
                    game.level.state.global.activeUnitsCountChanged = true;

                    game.level.deadPeople.push(this.state.profile.name);
                    game.sounder.play('death');
                    this.animations.play('die');
                    this.dropLight(this.state.eatenBy);
                    break;
                case STATE_PANIC:
                    this.definitelyPanic();
                    break;
            }
        });

        this.state.onChange(p => p.rotation, rotation => {
            this.rotation = rotation;
        });

        this.moveNoises = game.level.noise.createEmitter(this, 1000, NOISE_HUMAN, LOUD_STEP);
        this.attractionNoiser = game.level.noise.createEmitter(this, -1, NOISE_ATTENTION, LOUD_ATTENTION);

        game.level.state.global.onChange(g => g.anybodyMoved, () => {
            this.state.allowRandomNoise = true;
        });

        this.state.onChange(p => p.seesMonsterRightNow, seesMonsterRightNow => {
            if (seesMonsterRightNow) {
                this.maybePanic();
            }
        });

        this.mover = new Mover(this, game.level.passMap.findUnitPath.bind(game.level.passMap));

        this._idleRotationCooldown = game.rnd.integerInRange(...TimeBetweenIdleRotation);

        this._speed = FULL_SPEED;


        this._flash = game.add.sprite(0, 0, "human", type + "_flash.png");
        this._flash.anchor.set(0.5, 0.5);
        this._flash.visible = false;

        this._panicIcon = game.add.sprite(0, 0, "UI", "panic.png");
        this._panicIcon.visible = false;
        this._panicIcon.anchor.set(0.5, 1);
        this._panicIcon.update = () => {
            this._panicIcon.x = this.centerX;
            this._panicIcon.y = this.centerY - 10;
        }

        this.state.onChange(s => s.selected, this.updateSelection.bind(this));
    }

    definitelyPanic() {
        this._panicIcon.visible = true;
        this.game.sounder.play('panic');
        let p = game.level.passMap.getPassablePointInRadius(this.state.x, this.state.y, PIX * 10, PIX * 20, true);
        if (!p) p = game.level.passMap.getPassablePointInRadius(this.state.x, this.state.y, PIX, PIX * 10, true);
        if (p) {
            Unit.unitMoveTo(this.state, p);
        }
    }

    maybePanic() {
        if (this.state.mainState !== STATE_NORMAL) return; //we are panicing. or dying. cannot panic when dying.
        let shallPanic = game.rnd.frac() < 0.5; //maybe use some stat further
        if (shallPanic) {
            this.state.mainState = STATE_PANIC;
            this.state.panicTimeout = PanicTimeout;
        }
    }

    static canExecuteOrder(unitState) {
        return unitState.mainState === STATE_NORMAL;
    }

    static canBeSelected(unitState) {
        return unitState.mainState === STATE_NORMAL || unitState.mainState === STATE_PANIC || unitState.mainState === STATE_DYING;
    }

    static canDropShadow(unitState) {
        return unitState.mainState === STATE_NORMAL || unitState.mainState === STATE_PANIC;
    }

    updateMovement() {
        if (this.state.movement.moving) {
            game.level.state.global.anybodyMoved = true;
            this.moveNoises.emit();
        }
        this.mover.updateMovement();
    }

    setSelected(val) {
        showSelection(this, SELECTION_GREEN, 0, val);
        this.selected = true; // <- camera uses this //todo ruslan ->> still uses?

        this.state.selected = val;
    }

    updateSelection() {
        if (!this.state.selected) {
            showSelection(this, SELECTION_GREEN, 0, false);
        } else {
            switch (this.state.mainState) {
                case STATE_NORMAL:
                    showSelection(this, SELECTION_GREEN, 0, true);
                    break;
                case STATE_PANIC:
                    showSelection(this, SELECTION_RED, 0, true);
                    break;
                case STATE_DYING:
                case STATE_EVACUATED:
                    showSelection(this, SELECTION_GREEN, 0, false);
                    break;

            }
        }
    }

    updateLight() {
        if (this.state.mainState === STATE_DYING) {
            //something else
            this.state.light.dyingTime += game.time.physicsElapsedMS;
        } else {
            this._lightP = this._lightP || new Phaser.Point();
            this._lightP.set(+10, -10);
            Phaser.Point.rotate(this._lightP, 0, 0, this.state.rotation);
            this.state.light.x = this.state.x + this._lightP.x;
            this.state.light.y = this.state.y + this._lightP.y;
        }
        this._flash.x = this.state.light.x;
        this._flash.y = this.state.light.y
    }

    dropLight(monsterSprite = this) {

        let vector = new Phaser.Point(this.state.light.x - monsterSprite.x, this.state.light.y - monsterSprite.y);

        vector.setMagnitude(PIX);

        game.add.tween(this.state.light).to({
            x: this.state.light.x + vector.x,
            y: this.state.light.y + vector.y
        }, 300, Phaser.Easing.Bounce.Out, true);
        this._flash.visible = true;

    }

    updateRandomSound() {
        if (!this.state.allowRandomNoise) return;
        this.state.timeToRandomSound -= game.time.physicsElapsedMS;
        if (this.state.timeToRandomSound < 0) {
            this.state.timeToRandomSound = game.rnd.integerInRange(...TimeBetweenRandomSoundsRange);

            if (game.rnd.frac() < RandomSoundChancePerNeighbours[this.state.neighbours]) {
                let type = game.rnd.pick([LOUD_SNEEZE, LOUD_SCREAM, LOUD_STEP]);
                switch (type) {
                    case LOUD_SNEEZE: game.sounder.play('sneeze'); break;
                    case LOUD_SCREAM: game.sounder.play('gasp'); break;
                    case LOUD_STEP: game.sounder.play('fall'); break;
                }
                game.level.noise.emitNoise(this.x, this.y, NOISE_HUMAN, type);
            }
        }
    }

    update() {
        super.update();

        this.updateLight();
        switch (this.state.mainState) {
            case STATE_PANIC:
                this.state.panicTimeout -= game.time.physicsElapsedMS;
                if (this.state.panicTimeout < 0) {
                    this.state.mainState = STATE_NORMAL;
                }
                this.updateMovement();
                break;
            case STATE_DYING:
                //nothing;
                break;
            default:
                this.updateMovement();
                break;
        }

        if (!this.state.movement.moving && this.state.mainState === STATE_NORMAL) {
            switch (this.state.substate) {
                case SUBSTATE_IDLE:
                    this.updateRandomSound();
                    this._idleRotation();
                    break;
                case SUBSTATE_NOISING:
                    this.state.timeToNoise -= game.time.physicsElapsedMS;
                    if (this.state.timeToNoise < 0) {
                        this.animations.play("noising");
                        this.attractionNoiser.emit();
                        this.state.timeToNoise = NoiseEach;
                    }
                    break;
            }
        }

        this._sillySpread();
        this._turnToTheMovementDirection();

    }

    setBeingEated(monsterSprite) {
        this.stop();
        this.state.mainState = STATE_DYING;
        this.state.eatenBy = monsterSprite;
        this.state.x = monsterSprite.state.x;
        this.state.y = monsterSprite.state.y;
    }

    get pixPerMs() {
        return this._speed * (this.state.mainState === STATE_PANIC ? PANIC_BOOST : 1);
    }

    static unitMoveTo(unitState, target) {
        unitState.movement.target = target;
    }

    static doNoise(unitState) {
        unitState.noiseRequested = true;
    }

    static doNoNoise(unitState) {
        if (unitState.substate === SUBSTATE_NOISING) {
            unitState.substate = SUBSTATE_IDLE;
        }
    }

    static unitResetPath(unitState) {
        unitState.movement.path = undefined;
    }

    static unitStop(unitState) {
        unitState.movement.velocity.x = 0;
        unitState.movement.velocity.y = 0;
        unitState.movement.moving = false;
        unitState.movement.target = undefined;
        unitState.movement.path = undefined;
    }

    stop() {
        this.mover.stop();
        this.walkSound.stop();
    }

    destroy() {
        super.destroy();
        this.walkSound.stop();
    }

    static unitEvacuated(unitState) {
        unitState.mainState = STATE_EVACUATED;
    }

    _sillySpread() {
        let r = PIX * 0.75;
        let force = 0.3;

        for (let unit of game.level.unitsGrp.children) {
            if (this.state !== unit.state && Phaser.Point.distance(this.state, unit.state) < r) {
                let vector = Phaser.Point.subtract(this.state, unit.state).normalize();
                let nextX = this.state.x + vector.x * force;
                let nextY = this.state.y + vector.y * force;

                if (!game.level.passMap.cellIsBlockedAtPoint(nextX, nextY)) {
                    this.state.x = nextX;
                    this.state.y = nextY;
                }
            }
        }
    }

    _idleRotation() {
        this._idleRotationCooldown -= game.time.physicsElapsedMS;

        if (this._idleRotationCooldown <= 0) {
            this._idleRotationCooldown = game.rnd.integerInRange(...TimeBetweenIdleRotation);
            this._rotationSpeed = Math.PI / 8;

            this._desiredRotation = game.rnd.frac() < 0.5 ? this.rotation - Math.PI / 2 : this.rotation + Math.PI / 2;
        }
        this._updateDesiredRotation();
    }

    _turnToTheMovementDirection() {
        this._rotationSpeed = 0.3;

        if (this.state.movement.target) {
            let velocity = this.state.movement.velocity;
            let speed = Math.sqrt(velocity.x * velocity.x + velocity.y * velocity.y);
            if (speed > 0) {
                this._desiredRotation = Math.atan2(velocity.y, velocity.x);// Math.sign(velocity.y) * Math.acos(velocity.x / speed);
            }

            let shortest = Math.abs(Phaser.Math.getShortestAngle(Phaser.Math.radToDeg(this._desiredRotation), this.angle));
            if (shortest > 90) this._speed = FULL_SPEED * 0.1;
            else if (shortest > 45) this._speed = FULL_SPEED * 0.3;
            else if (shortest > 30) this._speed = FULL_SPEED * 0.5;
            else this._speed = FULL_SPEED;

            this._updateDesiredRotation();
        }
    }

    get canBeGrabbed() {
        return this.state.mainState !== STATE_DYING && this.state.mainState !== STATE_EVACUATED;
    }

    _updateDesiredRotation() {
        this.state.rotation = Phaser.Math.rotateToAngle(this.rotation, this._desiredRotation, this._rotationSpeed);
    }

    static unitSeesMonster(unitState, sees) {
        unitState.seesMonsterRightNow = sees;
    }
}
