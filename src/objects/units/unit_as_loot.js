import StatefulSprite from "../StatefulSprite";
import { StateBase } from "../../utils/state";
import Unit from "./unit";
import { PIX } from "../../consts";
import { createProfile } from "./registry";

const POP_DISTANCE = PIX * 2;

// I HAVE NO IDEA WHY DO I NEED THIS
class UnitAsLootState extends StateBase {
    constructor() {
        super();
        this.postConstruct().save();
    }
}

export default class UnitAsLoot extends StatefulSprite {
    constructor(x, y) {

        let profile = createProfile();
        const type = profile.type;

        super(game, x, y, "human", type + "_dying4.png");

        this.anchor.set(0.5);
        this.setState(new UnitAsLootState());

        this.animations.add("resurrect", Phaser.Animation.generateFrameNames(type + '_dying', 4, 1, '.png'), 12);
        this.rotation = Math.random() * Math.PI * 2;
        this.profile = profile;
    }

    update() {
        for (let unit of game.level.unitsGrp.children) {
            if (Phaser.Point.distance(this, unit.state) < POP_DISTANCE) {
                this._resurrect();
                break;
            }
        }
    }

    _resurrect() {
        if (this._resurrected) return;
        this._resurrected = true;

        if (typeof ga === "function") ga('send', 'event', 'resurrect', game.level.levelName, this.profile.type);

        this.animations.play('resurrect').onComplete.addOnce(() => {
            let unit = new Unit(this.x, this.y, this.profile);
            game.level.unitsGrp.add(unit);
            //damn
            game.level.lightTexture.onAddUnit(unit);
            this.destroy()
        });
    }
}
