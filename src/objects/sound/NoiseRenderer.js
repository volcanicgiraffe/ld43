import {
    distanceWhenLessThanLimit,
    LOUD_MONSTER_LIMIT,
    NOISE_ATTENTION,
    NOISE_HUMAN,
    NOISE_MECHANICAL
} from "./NoiseManager";

const Map = {
    [NOISE_HUMAN]: "human",
    [NOISE_ATTENTION]: "attention",
    [NOISE_MECHANICAL]: "mec",
};

class Noise extends Phaser.Sprite {
    constructor(x, y, radius, type) {
        super(game, x, y);
        game.level.noiseGrp.add(this);
        this.text = game.add.text(0, 0, Map[type] + " noise!", {
            font: "Arial 8px"
        });
        this.addChild(this.text);
        game.add.tween(this).to({y: y-30}, 400, null, true).onComplete.addOnce(() => {
            this.destroy();
        })
    }

    destroy() {
        super.destroy(true);
    }
}


export default class NoiseRenderer {
    constructor() {
        this.lastRenderedId = -1;
        game.level.noise.state.onChange(s => s.emittedNoises, sounds => {
            for (let s of sounds) {
                if (s.id > this.lastRenderedId) {
                    this.lastRenderedId = s.id;
                    new Noise(s.x, s.y, distanceWhenLessThanLimit(s, LOUD_MONSTER_LIMIT), s.type);
                }
            }
        })
    }
}