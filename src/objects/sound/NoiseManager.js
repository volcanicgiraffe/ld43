import {StateBase} from "../../utils/state";

export const NOISE_HUMAN = "s.human";
export const NOISE_MECHANICAL = "s.mechanical";
export const NOISE_ATTENTION = "s.attention";

export const LOUD_STEP = 4;
export const LOUD_KICK = 8;
export const LOUD_SNEEZE = 8;
export const LOUD_SCREAM = 20;
export const LOUD_ATTENTION = 20;
export const LOUD_FAN = 10;

export const LOUD_MONSTER_LIMIT = 0.1;

let SoundId = 1;

const SoundDistance1 = 32;

function loudnessOnDistance(sound, distance) {
    return sound.loudness * SoundDistance1/distance;
    //todo: try this
    //return sound.loudness * Math.sqrt(SoundDistance1/distance);
}

//so it is /2 on 2 squares, /3 on 3 squares... maybe need another progression
export function distantLoudness(sound, listenerX, listenerY) {
    let distance = Math.max(SoundDistance1, Math.hypot(listenerX - sound.x, listenerY - sound.y));
    return loudnessOnDistance(sound, distance);
}

export function distanceWhenLessThanLimit(sound, limit) {
    let max = 30;
    for (let i = 1; i < max; i++) {
        if (loudnessOnDistance(sound, i*SoundDistance1) < limit) return i*SoundDistance1;
    }
    return max*SoundDistance1;
}

class EmittedNoise {
    constructor(x, y, type, loudness) {
        this.id = SoundId++;
        this.x = x;
        this.y = y;
        this.type = type;
        this.loudness = loudness;
        this.unique = type + "_" + (x |0)+ "_" + (y|0);
    }
}


class NoiseState extends StateBase {
    constructor() {
        super("sound");
        this.emittedNoises = [];
        this.postConstruct();
    }

    onAddNoise(noise) {
        this.emittedNoises = this.emittedNoises.concat([noise]);
    }

    resetState() {
        this.emittedNoises = [];
    }
}


class NoiseListenerState extends StateBase {
    constructor() {
        super("noiseListener");
        this.listenedNoises = [];
        this.listenedNoisesUnique = {};
        this.postConstruct();
    }
}

class NoiseListener {
    constructor(sprite, loudnessLimit) {
        this.sprite = sprite;
        this.loudnessLimit = loudnessLimit;
        this.state = new NoiseListenerState();
    }

    onNoise(noise) {
        if (this.state.listenedNoisesUnique[noise.unique]) return;

        let realLoudness = distantLoudness(noise, this.sprite.x, this.sprite.y);
        let soundObj = Object.assign({}, noise, {loudness: realLoudness});
        this.state.listenedNoisesUnique = Object.assign(this.state.listenedNoisesUnique, {[noise.unique]: soundObj});
        if (realLoudness > this.loudnessLimit) {
            this.state.listenedNoises = this.state.listenedNoises.concat([soundObj]);
        }
    }

    getMostLoudNoise() {
        this.state.listenedNoises.sort((s1, s2) => s2.loudness - s1.loudness);
        let most = this.state.listenedNoises[0];
        //console.log(this.state.listenedNoises.map(ln => ln.unique + "-> " + ln.loudness));
        this.state.listenedNoises = [];
        this.state.listenedNoisesUnique = {};

        return most;
    }
}


export default class NoiseManager {

    constructor() {
        this.state = new NoiseState();
        this.listeners = [];
    }

    emitNoise(x, y, type, loudness) {
        let noise = new EmittedNoise(x, y, type, loudness);
        this.state.onAddNoise(noise);
        for (let l of this.listeners) {
            l.onNoise(noise);
        }
        //console.log("noise", noise);
    }

    createEmitter(sprite, eachMs, type, loudness) {
        let emitTimeout = 0;
        return {
            emit: (aType = type, aLoudness = loudness) => {
                if (eachMs === -1 || emitTimeout <= 0) {
                    this.emitNoise(sprite.centerX, sprite.centerY, aType, aLoudness);
                    emitTimeout = eachMs;
                } else {
                    emitTimeout -= game.time.physicsElapsedMS;
                }
            }
        }
    }

    addListener(sprite, loudnessLimit) {
        let listener = new NoiseListener(sprite, loudnessLimit);
        this.listeners.push(listener);
        sprite.events.onDestroy.addOnce(() => {
            this.listeners = this.listeners.filter(l => l.sprite !== sprite);
        });
        return listener;
    }

    afterUpdate() {
        this.state.checkChanges();
        this.state.resetState();
    }

}