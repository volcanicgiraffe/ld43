export default class StatefulSprite extends Phaser.Sprite {
    setState(state) {
        this.state = state;
        game.level.state.sprites[this.state.id] = this;
        if (game.DEBUG_STATE) {
            this.inputEnabled = true;
        }
    }

    update() {
        this.state.checkChanges();
        if (game.DEBUG_STATE && this.alive) {
            if (this.input.justPressed() && !this._debugWrote) {
                // console.log(this.state.id, this.state);
                this._debugWrote = true;
            }
            if (this.input.justOut()) {
                this._debugWrote = false;
            }
        }
        for (let c of this.children) c.update();
    }

    destroy(...args) {
        super.destroy(...args);
        delete game.level.state.sprites[this.state.id];
    }
}
