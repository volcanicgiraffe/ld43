//assumes that sprite has state.movement
import Unit from "./units/unit";

export default class Mover {

    constructor(sprite, pathFindFn) {
        this.sprite = sprite;
        this.state = sprite.state;
        this.pathFindFn = pathFindFn;

        /*this.state.onChange(p => p.movement.moving, target => {
            if (this.sprite.state.id === "monster1") console.trace('moving is', target, this.sprite);

        });*/
        this.state.onChange(p => p.movement.target, target => {
            this.state.movement.moving = !!target;
            this.state.movement.path = undefined;
            //if (this.sprite.state.id === "monster1") console.trace('target is', target, this.sprite);
        });
    }

    updateMovement() {
        if (this.state.movement.moving) {
            let target = this.state.movement.target;
            let pos = this.state;

            let path = this.state.movement.path;
            if (!path || !path.length) {
                if (!target) {
                    console.warn("no target! alexey, fiksi");
                    this.stop();
                    return;
                }
                this.state.movement.path = path = this.pathFindFn(pos.x, pos.y, target.x, target.y);
                //console.log(this.state.id, "new path", path);
                if (!path.length) {
                    //no way!!!
                    this.stop();
                    return;
                }
            }
            this._point = this._point || new Phaser.Point();
            let nextPoint = path[0];
            this._point.set(nextPoint.x - pos.x, nextPoint.y - pos.y);
            let distToTarget = this._point.getMagnitude();
            while (distToTarget < 2 && path.length > 1) {
                path.shift();
                nextPoint = path[0];
                this._point.set(nextPoint.x - pos.x, nextPoint.y - pos.y);
                distToTarget = this._point.getMagnitude();
            }
            //console.log(this.state.id, "pos", pos.x, pos.y, " -> ", nextPoint.x, nextPoint.y);
            if (distToTarget < 2) {
                //reached last point
                this.stop();
            } else {
                //for monster only!!!
                this.state.movement.doorOnWay = game.level.passMap.findDoorForward(this.sprite.x, this.sprite.y, nextPoint.x, nextPoint.y);
                this._point.normalize().setMagnitude(Math.min(distToTarget, this.sprite.pixPerMs));
                this.state.movement.velocity.x = this._point.x;
                this.state.movement.velocity.y = this._point.y;
            }
        }
        this.state.x += this.state.movement.velocity.x*game.time.physicsElapsedMS;
        this.state.y += this.state.movement.velocity.y*game.time.physicsElapsedMS;

        this.sprite.x = this.state.x;
        this.sprite.y = this.state.y;

    }

    stop() {
        //console.log(this.state.id, "stop");
        Unit.unitStop(this.state);
    }
}