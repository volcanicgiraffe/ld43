export default class ClickPointer extends Phaser.Sprite {
    constructor() {
        super(game, -100, -100);

        this.circle = game.add.graphics(0, 0);
        this.circle.scale.set(0.0);
        this.circle.beginFill(0xFFE8AA, 1);
        this.circle.drawCircle(0, 0, 15);
        this.circle.anchor.set(0.5);
        this.alpha = 0.4;

        this.addChild(this.circle);
        this.anchor.set(0.5);
    }

    onClick(point) {
        this.circle.scale.set(0.0);
        this.x = point.x;
        this.y = point.y;
        game.add.tween(this.circle.scale).to({ x: 1.0, y: 1.0 }, 150, Phaser.Easing.Cubic.InOut, true, 0, 0, true);
    }
}
