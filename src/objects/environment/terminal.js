import StatefulSprite from "../StatefulSprite";
import { StateBase } from "../../utils/state";

class TerminalState extends StateBase {
    constructor(targetId) {
        super(undefined, "console");
        this.enabled = false;
        this.targetId = targetId;
        this.postConstruct().save();
    }
}

export default class Terminal extends StatefulSprite {
    constructor(x, y, targetIds = [], orientation = 'D') {
        super(game, x, y, "objects", "console_off1.png");
        this.setOrientation(orientation);
        this.animations.add("disabled", Phaser.Animation.generateFrameNames('console_off', 1, 7, '.png'), 6, true);
        this.animations.add("enabled", ["console_on.png"], 6);
        this.animations.play('disabled');
        this.setState(new TerminalState());
        this.state.onChange(n => n.enabled, enabled => {
            if (enabled) {
                this.animations.play("enabled");
            } else {
                this.animations.play("disabled");
            }
            targetIds.forEach(tid => {
                if (!game.level.state.objects[tid]) return;

                game.sounder.play('boop_1');

                if (game.level.state.objects[tid].turnedOnCount > 0) {
                    game.level.state.objects[tid].turnedOnCount = 0;
                } else {
                    game.level.state.objects[tid].turnedOnCount = 1;
                }
            });
        });
        game.level.lightTexture.registerObjectWithLights(this, "console_on.png");
    }

    setOrientation(o) {
        this.anchor.set(0.5, 0.5);
        this.x += this.width * 0.5;
        this.y += this.height * 0.5;
        const wallOffset = 10; // fuckit
        if (o === 'U') {
            this.angle = 180;
            this.y += wallOffset;
        } else if (o === 'L') {
            this.angle = 90;
            this.x += wallOffset;
        } else if (o === 'R') {
            this.angle = -90;
            this.x -= wallOffset;
        } else {
            this.angle = 0;
            this.y -= wallOffset;
        }
    }

    get interactive() {
        return true;
    }

    enable(val) {
        this.state.enabled = val;
    }
}
