import StatefulSprite from "../StatefulSprite";
import { StateBase } from "../../utils/state";
import { PIX } from "../../consts";

class DoorState extends StateBase {
    constructor(id, opened = false, strong = false, health = 100, inverse = false) {
        super(id);
        this.openedAtStart = opened;
        this.opened = opened;
        this.strong = strong;
        this.broken = false;
        this.inverse = inverse;
        this.turnedOnCount = opened ? 1 : 0;
        this.health = health;
        this.postConstruct().save();
    }
}

function detectPrefix(width, height, opened, strong) {
    let prefix = 2;
    if (width === 96 || height === 96) {
        prefix = 3;
    }
    if (width === 128 || height === 128) {
        prefix = 4;
    }
    return prefix;
}

function detectFrame(width, height, opened, strong) {
    let prefix = detectPrefix(width, height, opened, strong);
    return 'door' + prefix + (opened ? '_o2.png' : '_c.png');
}

export default class Door extends StatefulSprite {
    constructor(x, y, width, height, id, opened, strong, health, inverse) {
        super(game, x, y, "objects", detectFrame(width, height, opened, strong));

        if (width < height) {
            this.angle = 90;
            this.anchor.set(0, 1);
        }
        let prefix = detectPrefix(width, height, opened, strong);

        this.colBox = new Phaser.Rectangle(x, y, width, height);

        this.animations.add('open', ['door' + prefix + '_o1.png', 'door' + prefix + '_o2.png'], 12, false);
        this.animations.add('close', ['door' + prefix + '_o2.png', 'door' + prefix + '_o1.png', 'door' + prefix + '_c.png'], 12, false);
        this.setState(new DoorState(id, opened, strong, health, inverse));

        this.state.onChange(n => n.opened, opened => {
            game.level.state.global.openedDoorsChanged = true;
            if (opened) {
                this.animations.play('open');
                game.sounder.play('door');
            } else {
                this.animations.play('close');
                game.sounder.play('door_close');
            }
        });

        game.level.passMap.setDoorSprite(this, strong); //todo: maybe somewhere else?

        this.state.onChange(p => p.turnedOnCount, newVal => {
            if (this.state.broken) return;
            this.state.opened = !this.state.inverse ? newVal > 0 : newVal === 0;
        });
        this.state.onChange(p => p.health, health => {
            if (health < 0) {
                this.state.broken = true;
                this.state.opened = true;
            }
        });

        game.level.lightTexture.registerObjectWithLights(this);

    }

    damageDoor(hp) {
        this.state.health -= hp;
    }

    getMiddlePointBefore(sprite) {
        let p = {};
        if (this.colBox.width > this.colBox.height) {
            //hor
            p.x = this.colBox.x + this.colBox.width / 2;
            p.y = sprite.y < this.colBox.y ? this.colBox.y - PIX : this.colBox.y + this.colBox.height + PIX;
        } else {
            //ver
            p.y = this.colBox.y + this.colBox.height / 2;
            p.x = sprite.x < this.colBox.x ? this.colBox.x - PIX : this.colBox.x + this.colBox.width + PIX;
        }
        return p;
    }
}
