export default class Wall extends Phaser.Sprite {
    constructor(x, y) {
        super(game, x, y, "test", 6);
        game.level.passMap.setWallSprite(this);
    }
}