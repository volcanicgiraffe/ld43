import StatefulSprite from "../StatefulSprite";
import {StateBase} from "../../utils/state";

class PressButtonState extends StateBase {
    constructor(targetId) {
        super(undefined, "button");
        this.pressed = false;
        this.targetId = targetId;
        this.postConstruct().save();
    }
}

export default class PressButton extends StatefulSprite {
    constructor(x, y, targetIds = []) {
        super(game, x, y, "objects", "switch_b_off.png");
        this.animations.add("push", ["switch_b_on.png"], 12);
        this.animations.add("pop", ["switch_b_off.png"], 12);

        this.setState(new PressButtonState());
        this.state.onChange(n => n.pressed, newPressed => {
           if (newPressed) {
               this.animations.play("push");
           }  else {
               this.animations.play("pop");
           }


           targetIds.forEach(tid => {
               game.level.state.objects[tid].turnedOnCount += newPressed ? 1 : -1;

               // the reason for this is to close the door if it's set 'open: true' in tiled and someone touched the button
               if (game.level.state.objects[tid].openedAtStart && !newPressed) {
                   game.level.state.objects[tid].openedAtStart = false;
                   game.level.state.objects[tid].turnedOnCount = 0;
               }
           });
        });

        game.level.lightTexture.registerObjectWithLights(this);
    }

    get interactive() {
        return true;
    }

    get canBePressed() {
        return true;
    }

    setPressed(val) {
        this.state.pressed = val;
    }
}
