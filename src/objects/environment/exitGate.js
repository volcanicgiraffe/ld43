import StatefulSprite from "../StatefulSprite";
import { StateBase } from "../../utils/state";

class ExitGateState extends StateBase {
    constructor() {
        super(undefined, "exitGate");
        this.postConstruct().save();
    }
}

export default class ExitGate extends StatefulSprite {
    constructor(x, y, orientation = 'D') {
        super(game, x, y, "objects", "exit_gate.png");
        this.setOrientation(orientation);
        this.setState(new ExitGateState());

        game.level.lightTexture.registerObjectWithLights(this);
    }

    setOrientation(o) {
        this.anchor.set(0.5, 0.5);
        this.x += this.width * 0.5;
        this.y += this.height * 0.5;
        const wallOffset = this.height * 0.8; // fuckit
        if (o === 'U') {
            this.angle = 180;
            this.x -= 45;
            this.y -= 12;
        } else if (o === 'L') {
            this.angle = 90;
            this.y -= 12;
            this.x -= wallOffset;
        } else if (o === 'R') {
            this.angle = -90;
            this.y -= 12;
            this.x -= wallOffset;
        } else {
            this.angle = 0;
            this.x -= wallOffset;
        }
    }
}