import StatefulSprite from "../StatefulSprite";
import { StateBase } from "../../utils/state";
import { prettify } from "../../helpers/text_utils";
import Unit from "../units/unit";

// I HAVE NO IDEA WHY DO I NEED THIS
class ExitZoneState extends StateBase {
    constructor(id) {
        super(id);
        this.postConstruct().save();
    }
}

export default class ExitZone extends StatefulSprite {
    constructor(x, y, width, height, id) {
        y += height / 2;
        x += width / 2;

        super(game, x, y, "test", 6);
        this.setState(new ExitZoneState(id));

        this.bitmap = game.add.bitmapData(width, height);
        this.bitmap.rect(0, 0, width, height, "#5295E4");

        this.loadTexture(this.bitmap);
        this.alpha = 0.3;
        game.add.tween(this).to({ alpha: 0.2 }, 400, Phaser.Easing.Cubic.InOut, true, 0, -1, true);

        this.anchor.set(0.5, 0.5);

        this.rect = new Phaser.Rectangle(x - width * 0.5, y - height * 0.5, width, height);
        this.peopleEvacuating = [];
        this._triggered = false;

        this._initTooltip();
    }

    _initTooltip() {
        this._tooltip = game.add.text(0, -this.height / 2 - 10, 'EVACUATE!');
        prettify(this._tooltip, 32, '#333333', '#99FFFF', 4);
        this._tooltip.alpha = 0;
        this._tooltip.anchor.set(0.5);
        this.addChild(this._tooltip);

        this._tooltip.rotation = -Math.PI / 64;
        game.add.tween(this._tooltip).to({ rotation: Math.PI / 64 }, 1200, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
    }

    update() {
        super.update();

        this.peopleEvacuating = [];

        for (let unit of game.level.unitsGrp.children) {

            if (this.rect.contains(unit.x, unit.y)) {
                this.peopleEvacuating.push(unit.state.profile);
                unit.state.evacuated = true;
            } else {
                unit.state.evacuated = false;
                unit._exitTimer = undefined;
            }
        }
        game.level.state.global.unitsInExitZoneCount = this.peopleEvacuating.length;
        if (!this._triggered && this.peopleEvacuating.length > 0) {
            this._triggered = true;
            this.tint = 0x55FF55;
            game.add.tween(this.scale).to({ x: 1.05, y: 1.05 }, 200, Phaser.Easing.Cubic.InOut, true, 0, 0, true);

            this._tooltip.alpha = 1.0;
        }
        if (this._triggered && this.peopleEvacuating.length === 0) {
            this._triggered = false;
            this.tint = 0xFFFFFF;
            game.add.tween(this.scale).to({ x: 0.95, y: 0.95 }, 200, Phaser.Easing.Cubic.InOut, true, 0, 0, true);

            this._tooltip.alpha = 0.0;
        }
    }

    destroy() {
        this.bitmap.destroy();
        super.destroy();
    }
}
