
import StatefulSprite from "../StatefulSprite";
import {StateBase} from "../../utils/state";

class SignState extends StateBase {
    constructor(letter, targetId) {
        super("sign-" + letter, "sign");
        this.turnedOn = false;
        this.targetId = targetId;
        this.postConstruct().save();
    }
}

export default class Sign extends StatefulSprite {
    constructor(x, y, width, height, letter, targetId, onlyLetter = false) {
        super(game, x+width/2, y + height/2, "objects", "tab_base.png");
        this.anchor.set(0.5, 0.5);
        if (onlyLetter) this.visible = false;

        this._letter = game.add.sprite(0, 0, "objects", "tab_" + letter + ".png");
        this._letter.anchor.set(0.5, 0.5);
        this._letter.y = this.y;
        this._letter.x = this.x;
        this.setState(new SignState(letter, targetId));
        if (targetId) {
            let targetState = game.level.state.objects[targetId];
            targetState.onChange(o => o.turnedOnCount, value => {
                this.state.turnedOn = targetState.opened === undefined ? !!value : targetState.opened;
            });
            this.state.turnedOn = targetState.opened === undefined ? !!value : targetState.opened;

            this.state.onChange(o => o.turnedOn, value => {
                //todo: switch sprites
            });
        }

        game.level.lightTexture.registerObjectWithLights(this._letter, undefined, true);

    }
}