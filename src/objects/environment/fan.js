import StatefulSprite from "../StatefulSprite";
import { StateBase } from "../../utils/state";
import { LOUD_FAN, NOISE_MECHANICAL } from "../sound/NoiseManager";

class FanState extends StateBase {
    constructor(id) {
        super(id);
        this.turnedOnCount = 0;
        this.postConstruct().save();
    }
}

export default class Fan extends StatefulSprite {
    constructor(x, y, id, isOn) {
        super(game, x, y, "objects", "vent_base.png");

        this.setState(new FanState(id));
        this.state.turnedOnCount = isOn ? 1 : 0;
        this.fanItself = game.add.sprite(0, 0, "objects", 'vent_rot.png');
        this.fanItself.anchor.set(0.5, 0.5);
        this.fanItself.x = this.fanItself.width / 2;
        this.fanItself.y = this.fanItself.height / 2;
        this.addChild(this.fanItself);

        this.fanSpeed = 0;

        this.state.onChange(n => n.turnedOnCount, newValue => {
            game.tweens.removeFrom(this);
            if (newValue > 0) {
                game.add.tween(this).to({ fanSpeed: 0.5 }, 2000, null, true);
            } else {
                game.add.tween(this).to({ fanSpeed: 0 }, 2000, null, true);
            }
        });

        this.noiseMaker = game.level.noise.createEmitter(this, 500, NOISE_MECHANICAL, LOUD_FAN);
        if (isOn) { this.state.turnedOnCount = 1; }

        game.level.lightTexture.registerObjectWithLights(this);
    }

    update() {
        super.update();
        this.fanItself.rotation = (this.fanItself.rotation + this.fanSpeed);
        if (this.state.turnedOnCount > 0) this.noiseMaker.emit();
    }
}
