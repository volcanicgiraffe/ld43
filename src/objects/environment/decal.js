export default class Decal extends Phaser.Sprite {
    constructor(x, y, name) {
        super(game, x, y, "decals", name + ".png");
        this.anchor.set(0.5);
        this.alpha = 0.5;
    }
}