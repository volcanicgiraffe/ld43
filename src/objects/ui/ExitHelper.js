export default class ExitHelper extends Phaser.Group {

    constructor() {
        super(game);

        this.goalPanel = game.add.sprite(game.canvas.width - 130, game.canvas.height - 10, "UI", "ui_base2.png");
        this.goalPanel.anchor.set(1, 1);
        this.add(this.goalPanel);

        this.goal = game.add.bitmapText(-20, -this.goalPanel.height+10, "font24", "Proceed to the exit zone", 22);
        this.goal.anchor.set(1, 0);
        this.goal.tint = 0x999999;
        this.goalPanel.addChild(this.goal);

        this.status = game.add.bitmapText(-20, -this.goalPanel.height+40, "font24", "No one is safe", 22);
        this.status.tint = 0xffcc00;
        this.status.anchor.set(1, 0);
        this.goalPanel.addChild(this.status);

        game.level.state.global.onChange(n => n.unitsInExitZoneCount, this.updateStatus.bind(this));

        this.buttonEvac = game.add.button(game.canvas.width - 10, game.canvas.height - 10, "UI", this.onEvac, this, "btn_evac_enabled.png", "btn_evac_enabled.png", "btn_evac_pressed.png", "btn_evac_enabled.png");
        this.buttonEvacDisabled = game.add.sprite(game.canvas.width - 10, game.canvas.height - 10, "UI", "btn_evac_disabled.png");
        this.buttonEvac.anchor.set(1, 1);
        this.buttonEvacDisabled.anchor.set(1, 1);

        this.add(this.buttonEvac);
        this.add(this.buttonEvacDisabled);
    }

    onEvac() {
        game.level.gameEnder.levelWin();
        this.stop = true;
    }

    updateStatus(newCount) {
        if (this.stop) return;
        this.status.text = newCount ? (newCount + " out of " + (game.level.state.objects.unit.length) + " in safety") : "No one is safe";
        this.status.tint = newCount ? 0x008811 : 0xbb3300;
        this.buttonEvac.visible = newCount;
        this.buttonEvacDisabled.visible = !newCount;
    }
}
