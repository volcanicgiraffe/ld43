import {STATE_DYING, STATE_NORMAL, STATE_PANIC, SUBSTATE_NOISING} from "../units/unit";
import Unit from "../units/unit";

export default class SelectedPanel extends Phaser.Group {

    constructor() {
        super(game);

        this.panelBg = game.add.sprite(40, game.canvas.height-10, "UI", "ui_base.png");
        this.panelBg.anchor.set(0, 1);
        this.add(this.panelBg);

        this.personBg = game.add.sprite(this.panelBg.left + 10, this.panelBg.top + 10, "UI", "portrait_base.png");
        this.add(this.personBg);


        this.portraitsGrp = game.add.group(this);

        this.portraitsGrp.x = this.personBg.centerX;
        this.portraitsGrp.y = this.personBg.bottom;

        this.nameLabel = game.add.bitmapText(this.panelBg.left + 170, this.panelBg.top + 60, "font36", "Name", 36, this);
        this.nameLabel.tint = 0xBBBBBB;

        this.statusLabel = game.add.bitmapText(this.panelBg.left + 170, this.panelBg.top + 100, "font24", "Status", 22, this);

        game.level.state.ui.onChange(n => n.selectedUnits, this.redrawSelectedUnits.bind(this));
        game.level.state.global.onEvent("anyStatusChanged", this.redrawSelectedUnits.bind(this));

        this.noiseButton = game.add.button(this.panelBg.right - 10, this.panelBg.top + 60, "UI", this.onNoiseClick, this, "btn_noise_hover.png", "btn_noise_off.png", "btn_noise_pressed.png", "btn_noise_off.png");
        this.noiseButton.anchor.set(1, 0);
        this.add(this.noiseButton);
        this.unnoiseButton = game.add.button(this.panelBg.right - 10, this.panelBg.top + 60, "UI", this.onUnnoiseClick, this, "btn_noise_pressed.png", "btn_noise_pressed.png", "btn_noise_off.png", "btn_noise_pressed.png");
        this.unnoiseButton.anchor.set(1, 0);
        this.add(this.unnoiseButton);
        this.redrawSelectedUnits();
    }


    onUnnoiseClick() {
        for (let id of game.level.state.ui.selectedUnits) {
            Unit.doNoNoise(game.level.state.objects[id]);
        }
        this.noiseButton.visible = true;
        this.unnoiseButton.visible = false;
    }

    onNoiseClick() {
        for (let id of game.level.state.ui.selectedUnits) {
            Unit.doNoise(game.level.state.objects[id]);
        }
        this.noiseButton.visible = false;
        this.unnoiseButton.visible = true;
    }

    createPortrait(unitState) {
        let grp = game.add.group();
        let shirt1 = game.add.sprite(2 , -20, "UI", unitState.profile.type + "_body.png");
        let face1 = game.add.sprite(-2, -55, "UI", unitState.profile.face);
        shirt1.anchor.set(0.5, 1);
        face1.anchor.set(0.5, 1);
        grp.add(shirt1);
        grp.add(face1);

        return grp;

    }

    redrawSelectedUnits() {
        let units = game.level.state.ui.selectedUnits.map(id => game.level.state.objects[id]);
        this.portraitsGrp.removeAll(true);

        const State2Status = {
            [STATE_NORMAL]: {text: "stable", tint: 0x008811},
            [STATE_PANIC]: {text: "PANIC", tint: 0xbb3300},
            [STATE_DYING]: {text: "R.I.P", tint: 0x444444}
        };

        if (units.length === 0) {
            //no one
            this.nameLabel.text = "No one selected";
            this.statusLabel.text = "";
            this.noiseButton.visible = this.unnoiseButton.visible = false;
        } else {
            this.portraitsGrp.add(this.createPortrait(units[0]));
            let allNoising = units.every(u => u.substate === SUBSTATE_NOISING);
            this.noiseButton.visible = !allNoising;
            this.unnoiseButton.visible = allNoising;
            if (units.length === 1) {
                this.nameLabel.text = units[0].profile.name;
                let {text, tint} = State2Status[units[0].mainState];
                this.statusLabel.text = text;
                this.statusLabel.tint = tint;
            } else {
                this.nameLabel.text = units.length + " survivors";
                this.statusLabel.text = "";
                let second = this.createPortrait(units[1]);
                second.x = 26;
                second.y = -35;
                second.scale.set(0.6);
                second.alpha = 0.4;
                this.portraitsGrp.addAt(second, 0);
                if (units.length > 2) {
                    let third = this.createPortrait(units[2]);
                    third.x = -30;
                    third.y = -42;
                    third.scale.set(0.4);
                    third.alpha = 0.3;
                    this.portraitsGrp.addAt(third, 0);
                }

            }
        }
    }
}