export function prettify(label, size = 12, fill = '#FFFFFF', strokeFill = '#000000', stroke = Math.round(size / 2)) {
    label.font = 'Arial';
    label.fontSize = size;
    label.fill = fill;
    label.stroke = strokeFill;
    label.strokeThickness = stroke;
    label.setShadow(2, 2, "#333333", 2, true, false);

    return label;
}
