export default class Alert extends Phaser.Sprite {
    constructor(x, y, type) {
        super(game, x, y, 'UI', type + '.png');
        this.alpha = 0;
        this.anchor.set(0.5);
        game.level.noiseGrp.add(this);
        game.add.tween(this).to({ alpha: 1 }, 300, Phaser.Easing.Linear.Out, true);
        game.add.tween(this).to({ y: y - 30 }, 2000, Phaser.Easing.Quadratic.InOut, true, 200);
        game.add.tween(this).to({ alpha: 0 }, 300, Phaser.Easing.Quadratic.In, true, 1800).onComplete.addOnce(() => this.destroy());
    }
}


export class AlertShower {
    constructor(pauseBetween = 1000) {
        this.timeToNext = 0;
        this.pauseBetween = pauseBetween;
        this.lastAlertType = undefined;
    }

    showAlert(x, y, type) {
        if (this.timeToNext <= 0 || this.lastAlertType !== type) {
            this.timeToNext = this.pauseBetween;
            this.lastAlertType = type;
            new Alert(x, y, type);
        }
    }

    update() {
        if (this.timeToNext > 0) {
            this.timeToNext -= game.time.physicsElapsedMS;
        }
    }
}