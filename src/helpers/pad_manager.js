export default class PadManager {

    constructor(phaserPad = game.input.gamepad.pad1) {

        this.pad = phaserPad;

        this.releasedX = true;
        this.releasedY = true;
        this.releasedA = true;
        this.releasedB = true;
        this.releasedStart = true;

        this.releasedLeft = true;
        this.releasedRight = true;
        this.releasedDown = true;
        this.releasedUp = true;
        this.pressedX = false;
        this.pressedY = false;
        this.pressedA = false;
        this.pressedB = false;
        this.pressedStart = false;
    }

    update() {
        if (!game.input.gamepad.supported || !game.input.gamepad.active || !this.pad.connected) return;

        this.pressedX = false;
        this.pressedY = false;
        this.pressedA = false;
        this.pressedB = false;
        this.pressedStart = false;

        if (!this.releasedX && this.pad.justReleased(Phaser.Gamepad.XBOX360_X)) {
            this.releasedX = true;
        }

        if (!this.releasedY && this.pad.justReleased(Phaser.Gamepad.XBOX360_Y)) {
            this.releasedY = true;
        }
        if (!this.releasedA && this.pad.justReleased(Phaser.Gamepad.XBOX360_A)) {
            this.releasedA = true;
        }
        if (!this.releasedB && this.pad.justReleased(Phaser.Gamepad.XBOX360_B)) {
            this.releasedB = true;
        }
        if (!this.releasedStart && this.pad.justReleased(Phaser.Gamepad.XBOX360_START)) {
            this.releasedStart = true;
        }

        if (!this.releasedLeft && this.pad.isUp(Phaser.Gamepad.XBOX360_DPAD_LEFT) && !this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X)) {
            this.releasedLeft = true;
            this.pressedLeft = false;
        }
        if (!this.releasedRight && this.pad.isUp(Phaser.Gamepad.XBOX360_DPAD_RIGHT) && !this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X)) {
            this.releasedRight = true;
            this.pressedRight = false;
        }
        if (this.pad.isUp(Phaser.Gamepad.XBOX360_DPAD_DOWN) && !this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y)) {
            this.releasedDown = true;
            this.pressedDown = false;
        }
        if (!this.releasedUp && this.pad.isUp(Phaser.Gamepad.XBOX360_DPAD_UP) && !this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y)) {
            this.releasedUp = true;
            this.pressedUp = false;
        }

        // PRESS

        if (this.releasedX && this.pad.justPressed(Phaser.Gamepad.XBOX360_X)) {
            this.releasedX = false;
            this.pressedX = true;
        }
        if (this.releasedY && this.pad.justPressed(Phaser.Gamepad.XBOX360_Y)) {
            this.releasedY = false;
            this.pressedY = true;
        }
        if (this.releasedA && this.pad.justPressed(Phaser.Gamepad.XBOX360_A)) {
            this.releasedA = false;
            this.pressedA = true;
        }
        if (this.releasedB && this.pad.justPressed(Phaser.Gamepad.XBOX360_B)) {
            this.releasedB = false;
            this.pressedB = true;
        }

        if (this.releasedStart && this.pad.justPressed(Phaser.Gamepad.XBOX360_START)) {
            this.releasedStart = false;
            this.pressedStart = true;
        }

        if (this.releasedLeft && (this.pad.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1)) {
            this.pressedLeft = true;
            this.releasedLeft = false;
        }
        if (this.releasedRight && (this.pad.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1)) {
            this.pressedRight = true;
            this.releasedRight = false;
        }
        if (this.releasedDown && (this.pad.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN) || this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) > 0.1)) {
            this.pressedDown = true;
            this.releasedDown = false;
        }
        if (this.releasedUp && (this.pad.isDown(Phaser.Gamepad.XBOX360_DPAD_UP) || this.pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) < -0.1)) {
            this.pressedUp = true;
            this.releasedUp = false;
        }
    }

    justDownX() {
        return this.pressedX;
    }

    justDownY() {
        return this.pressedY;
    }

    justDownA() {
        return this.pressedA;
    }

    justDownB() {
        return this.pressedB;
    }

    justDownStart() {
        return this.pressedStart;
    }

    justDownAny() {
        return this.pressedB || this.pressedA || this.pressedY || this.pressedX;
    }


    justDownLeft(releaseNow = false) {
        let justDown = this.pressedLeft;
        if (releaseNow) this.pressedLeft = false;
        return justDown;
    }

    justDownRight(releaseNow = false) {
        let justDown = this.pressedRight;
        if (releaseNow) this.pressedRight = false;
        return justDown;
    }

    justDownUp(releaseNow = false) {
        let justDown = this.pressedUp;
        if (releaseNow) this.pressedUp = false;
        return justDown;
    }

    justDownDown(releaseNow = false) {
        let justDown = this.pressedDown;
        if (releaseNow) this.pressedDown = false;
        return justDown;
    }
}
