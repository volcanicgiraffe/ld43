export default class Sounder {

    constructor() {
        this.groups = new Map();

        this.add('beat_1');
        this.add('beat_2');
        this.add('beat_3');

        this.add('boop_1');

        this.add('click_1', ['click_1'], 50);
        this.add('click', ['click2'], 50);
        this.add('loudnoise', ['loudnoise1', 'loudnoise2'], 50);
        this.add('panic', ['panic_1', 'gasp'], 4000);

        this.add('end_1');
        this.add('end_2');

        this.add('menu');

        this.add('spider_steps_hi');
        this.add('spider_steps_low');

        this.add('bang', ['bang_1', 'bang_2', 'bang_3'], 1000);
        this.add('sneeze', ['sneeze'], 1000);
        this.add('fall', ['fall'], 1000);
        this.add('gasp', ['gasp'], 1000);
        this.add('death', ['death_1', 'death_2', 'death_3'], 300);
        this.add('door', ['door_1', 'door_2', 'door_3'], 300);
        this.add('door_close', ['door_close_1', 'door_close_2', 'door_close_3'], 500);

        this.add('roar', ['roar_1', 'roar_2', 'roar_3', 'roar_4', 'roar_5'], 300);
        this.add('step_m', ['step_m_1', 'step_m_2', 'step_m_3'], 300);
        this.add('step_a', ['step_a_1', 'step_a_2', 'step_a_3'], 100);
        this.add('step_b', ['step_b_1', 'step_b_2', 'step_b_3'], 100);

    }

    destroy() {
        for (let [name, group] of this.groups.entries()) {
            group.sounds.forEach(s => s.destroy());
        }
    }

    add(group, sounds = [group], cooldown = 500) {
        this.groups.set(group, {
            sounds: sounds.map(name => this.addSound(name, group)),
            cooldown: cooldown,
            canPlay: true
        });
    }

    addSound(name, group) {
        let s = game.add.sound(name);
        s.allowMultiple = true;
        s.onPlay.add(() => {
            this.groups.get(group).canPlay = false;
            game.time.events.add(this.groups.get(group).cooldown, () => this.groups.get(group).canPlay = true);
        });
        return s;
    }

    play(group) {
        if (this.groups.get(group).canPlay) {
            game.rnd.pick(this.groups.get(group).sounds).play();
        }
    }

    resetAll() {
        for (let [name, group] of this.groups.entries()) {
            group.canPlay = true;
        }
    }
}
