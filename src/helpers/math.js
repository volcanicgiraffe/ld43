
export function reach(currentValue, targetValue, step) {
    if (Math.abs(targetValue - currentValue) < step) return targetValue;
    return currentValue + step*Math.sign(targetValue - currentValue);
}

export function toMmSs(ms) {
    if (ms === null || ms < 0) return "--:--";
    let seconds = ms / 1000;
    let pad = (input) => {
        return input < 10 ? "0" + input : input;
    };
    return [
        pad(Math.floor(seconds / 60)),
        pad(Math.floor(seconds % 60)),
    ].join(':');
}
