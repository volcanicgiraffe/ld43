const VERSION = 1;
const KEY_LEVEL = 'volcanic_giraffe.ld43.level';
const KEY_GAME_TIME = 'volcanic_giraffe.ld43.game_time';
const KEY_PROFILES = 'volcanic_giraffe.ld43.profiles';
const KEY_DEAD_LIST = 'volcanic_giraffe.ld43.dead_list';

const DEFAULT_PROFILES = [{}, {}, {}, {}, {}, {}, {}];
const DEFAULT_LEVEL_INDEX = 0;
const DEFAULT_DEAD_LIST = [];
const DEFAULT_GAME_TIME = 0;

export default class StorageManager {
    constructor() {
        if (silochMode()) {
            this._profiles = DEFAULT_PROFILES;
            this._levelIndex = DEFAULT_LEVEL_INDEX;
            this._gameTime = DEFAULT_GAME_TIME;
            this._deadList = DEFAULT_DEAD_LIST;
        } else {
            this._profiles = getSavedState(KEY_PROFILES, VERSION)['profiles'] || DEFAULT_PROFILES;
            this._levelIndex = getSavedState(KEY_LEVEL, VERSION)['levelIndex'] || DEFAULT_LEVEL_INDEX;
            this._gameTime = getSavedState(KEY_GAME_TIME, VERSION)['gameTime'] || DEFAULT_GAME_TIME;
            this._deadList = DEFAULT_DEAD_LIST;
        }
    }

    saveLevelIndex(index) {
        this._levelIndex = index;
        if (silochMode()) return;
        let state = getSavedState(KEY_LEVEL, VERSION);
        state['levelIndex'] = index;
        saveState(KEY_LEVEL, state);
    }

    loadLevelIndex() {
        return getSavedState(KEY_LEVEL, VERSION)['levelIndex'] || this._levelIndex;
    }


    saveGameTime(gameTime) {
        this._gameTime = gameTime;
        if (silochMode()) return;
        let state = getSavedState(KEY_GAME_TIME, VERSION);
        state['gameTime'] = gameTime;
        saveState(KEY_GAME_TIME, state);
    }

    loadGameTime() {
        return getSavedState(KEY_GAME_TIME, VERSION)['gameTime'] || this._gameTime;
    }

    saveProfiles(profiles) {
        this._profiles = profiles;
        if (silochMode()) return;
        let state = getSavedState(KEY_PROFILES, VERSION);
        state['profiles'] = profiles;
        saveState(KEY_PROFILES, state);
    }

    loadProfiles() {
        return getSavedState(KEY_PROFILES, VERSION)['profiles'] || this._profiles;
    }

    addToDeadList(names) {
        let deadList = this.loadDeadList();
        this.saveDeadList(deadList.concat(names))
    }

    saveDeadList(deadList) {
        this._deadList = deadList;
        if (silochMode()) return;
        let state = getSavedState(KEY_DEAD_LIST, VERSION);
        state['deadList'] = deadList;
        saveState(KEY_DEAD_LIST, state);
    }

    loadDeadList() {
        return getSavedState(KEY_DEAD_LIST, VERSION)['deadList'] || this._deadList
    }

    resetProgress() {
        this.saveDeadList(DEFAULT_DEAD_LIST);
        this.saveProfiles(DEFAULT_PROFILES);
        this.saveLevelIndex(DEFAULT_LEVEL_INDEX);
    }
}

export function createSavedState(version) {
    return { version: version }
}

export function saveState(key, state) {
    if (silochMode()) return;
    localStorage.setItem(key, JSON.stringify(state));
}

export function getSavedState(key, version) {
    if (silochMode()) return {};
    let s = localStorage.getItem(key);
    if (s === undefined || s === null) {
        s = createSavedState(version);
        saveState(key, s);
    } else {
        try {
            s = JSON.parse(s);
        } catch (e) {
            console.error("Cannot parse saved state due to error. Create empty", e);
            s = createSavedState(version);
            saveState(key, s);
        }
    }
    if (s.version !== version) {
        console.warn("Old save found, need migration");
        s = createSavedState(version);
        saveState(key, s);
    }
    return s;
}

export function silochMode() {
    try {
        localStorage.getItem("none");
        return false;
    } catch (e) {
        return true;
    }
}
