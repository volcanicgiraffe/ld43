export default class AnyInput {
    constructor() {
        this.cursors = game.input.keyboard.createCursorKeys();

        this.up = game.input.keyboard.addKey(Phaser.Keyboard.W);
        this.down = game.input.keyboard.addKey(Phaser.Keyboard.S);
        this.left = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.right = game.input.keyboard.addKey(Phaser.Keyboard.D);

        this.evacuate = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.menu = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        this.restart = game.input.keyboard.addKey(Phaser.Keyboard.R);
    }

    holdUp() {
        return this.up.isDown || this.cursors.up.isDown || game.pad.justDownUp();
    }

    holdDown() {
        return this.down.isDown || this.cursors.down.isDown || game.pad.justDownDown();
    }

    holdLeft() {
        return this.left.isDown || this.cursors.left.isDown || game.pad.justDownLeft();
    }

    holdRight() {
        return this.right.isDown || this.cursors.right.isDown || game.pad.justDownRight();
    }

    pressedEvacuate() {
        return this.evacuate.justPressed() || game.pad.justDownAny();
    }

    pressedMenu() {
        return this.menu.justPressed() || game.pad.justDownStart();
    }

    pressedRestart() {
        return this.restart.justPressed();
    }

}
