import 'pixi';
import 'p2';
import 'phaser';

import init from 'states/init'
import load from 'states/load'
import menu from 'states/menu'
import play from 'states/play'
import level_state from 'states/levels/level_state'
import win from 'states/win'

window.game = new Phaser.Game(1280, 720, Phaser.WEBGL, 'canvas', null);
//game.preserveDrawingBuffer = true;
game.antialias = false;

game.state.add('init', init);
game.state.add('load', load);
game.state.add('menu', menu);
game.state.add('play', play);
game.state.add('win', win);
game.state.add('level_state', level_state);

game.state.start('init');

let pollStatus = Phaser.SinglePad.prototype.pollStatus;
Phaser.SinglePad.prototype.pollStatus = function () {
    if (!this._rawPad) return;
    pollStatus.call(this);
};
